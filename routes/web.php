<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/Home',['as'=>'welcome','uses'=>'FrontendController@Home']);
Route::get('/TestMail',['as'=>'welcome','uses'=>'FrontendController@TestMail']);
Route::get('/AboutUs',['as'=>'Aboutus','uses'=>'FrontendController@AboutUs']);
Route::get('/BecomeMember', ['as'=>'BecomeMember','uses'=>'FrontendController@BecomeMember']);
Route::get('/MemberDirectory', ['as'=>'MemberDirectory','uses'=>'FrontendController@MemberDirectory']);
Route::get('/AboutTourismGala', ['as'=>'AboutTourismGala','uses'=>'FrontendController@AboutTourismGala']);
Route::get('/Nominate', ['as'=>'Nominate','uses'=>'FrontendController@Nominate']);
Route::get('/NominateNow', ['as'=>'NominateNow','uses'=>'FrontendController@NominateNow']);
Route::get('/MoreAwardCategories', ['as'=>'AwardCategories','uses'=>'FrontendController@AwardCategories']);
Route::post('/SendYourNominee', ['as'=>'SendYourNominee','uses'=>'FrontendController@SendYourNominee']);
Route::get('/VoteNow', ['as'=>'VoteNow','uses'=>'FrontendController@VoteNow']);
Route::get('/PeopleAwardCategory', ['as'=>'VoteNow','uses'=>'FrontendController@PeopleAwardCategory']);
Route::get('/AwardSubCategory', ['as'=>'AwardSubCategory','uses'=>'FrontendController@AwardSubCategory']);

Route::post('/SendYourBusinessNominee', ['as'=>'SendYourBusinessNominee','uses'=>'FrontendController@SendYourBusinessNominee']);
Route::get('/AwardsCategory', ['as'=>'AwardsCategory','uses'=>'FrontendController@AwardsCategory']);
Route::get('/VoteYourNominee', ['as'=>'VoteYourNominee','uses'=>'FrontendController@VoteYourNominee']);
Route::post('/SendVoterNumber', ['as'=>'SendVoterNumber','uses'=>'FrontendController@SendVoterNumber']);
Route::post('/SendVoterSmsCode', ['as'=>'SendVoterSmsCode','uses'=>'FrontendController@SendVoterSmsCode']);
Route::post('/VoteWithEmail', ['as'=>'VoteWithEmail','uses'=>'FrontendController@VoteWithEmail']);
Route::get('/LocationUser', ['as'=>'LocationUser','uses'=>'FrontendController@LocationUser']);
Route::get('/AwardCategory', ['as'=>'BusinessAwardCategory','uses'=>'FrontendController@BusinessAwardCategory']);
Route::get('/VoteNow', ['as'=>'BusinessAwardCategory','uses'=>'FrontendController@BusinessAwardCategory']);
Route::get('/TheProcess', ['as'=>'TheProcess','uses'=>'FrontendController@TheProcess']);
Route::get('/google259078c174a141f7.html',['as'=>'google259078c174a141f7.html','uses' =>'FrontendController@Google']);
Route::get('/sitemap.xml',['as'=>'sitemap.xml','uses' =>'FrontendController@SiteMap']);


Route::get('/News', ['as'=>'News','uses'=>'FrontendController@News']);
Route::get('/NewsReadMore', ['as'=>'NewsReadMore','uses'=>'FrontendController@NewsReadMore']);
Route::get('/Publications', ['as'=>'Publications','uses'=>'FrontendController@Publications']);
Route::get('/JoinsUs', ['as'=>'JoinsUs','uses'=>'FrontendController@BecomeMember']);
Route::get('/Thankyou', ['as'=>'Thankyou','uses'=>'FrontendController@Thankyou']);
Route::get('/VisitRwanda', ['as' => 'VisitRwanda', 'uses' => 'BackendController@VisitRwanda']);
Route::get('/AttractionsMore', ['as' => 'AttractionsMore', 'uses' => 'BackendController@AttractionsMore']);
Route::post('/Hireaguide', ['as' => 'Hireaguide', 'uses' => 'BackendController@Hireaguide']);

Route::get('/ContactUs', ['as'=>'ContactUs','uses'=>'FrontendController@ContactUs']);
Route::get('/Webmail', ['as'=>'Webmail','uses'=>'FrontendController@Webmail']);
Route::post('/HotelCategory', ['as' => 'backend.HotelCategory', 'uses' => 'FrontendController@HotelCategory']);
Route::post('/SubCategory', ['as' => 'backend.SubCategory', 'uses' => 'FrontendController@SubCategory']);


Route::get('/Test', ['as'=>'backend.Login','uses'=>'FrontendController@Login']);



Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => 'disablepreventback'],function(){
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/Dashboard', ['as' => 'backend.Dashboard', 'uses' => 'BackendController@Dashboard']);
        Route::get('/AccountList', ['as' => 'backend.AccountList', 'uses' => 'BackendController@AccountList']);
        Route::get('/CreateAccount', ['as' => 'backend.CreateAccount', 'uses' => 'BackendController@CreateAccount']);
        Route::post('/CreateAccount_', ['as' => 'backend.CreateAccount', 'uses' => 'BackendController@CreateAccount_']);
        Route::get('/ListofPeopleAward', ['as' => 'backend.ListofPeopleAward', 'uses' => 'BackendController@ListofPeopleAward']);
        Route::get('/ListOfBusinessAward', ['as' => 'backend.ListOfBusinessAward', 'uses' => 'BackendController@ListOfBusinessAward']);
        Route::post('/EditListofPeopleAward', ['as' => 'backend.EditListofPeopleAward', 'uses' => 'BackendController@EditListofPeopleAward']);
        Route::post('/ListOfBusinessAwardEdit', ['as' => 'backend.ListOfBusinessAwardEdit', 'uses' => 'BackendController@ListOfBusinessAwardEdit']);
        Route::post('/ListOfBusinessAwardFilter', ['as' => 'backend.ListOfBusinessAwardFilter', 'uses' => 'BackendController@ListOfBusinessAwardFilter']);
        Route::post('/ListofPeopleAwardFilter', ['as' => 'backend.ListofPeopleAwardFilter', 'uses' => 'BackendController@ListofPeopleAwardFilter']);
        Route::post('/NotifyMember', ['as' => 'backend.NotifyMember', 'uses' => 'BackendController@NotifyMember']);
        Route::get('/AwardCategories', ['as' => 'backend.AwardCategories', 'uses' => 'BackendController@AwardCategories']);
        Route::post('/AddAwardCategories', ['as' => 'backend.AddAwardCategories', 'uses' => 'BackendController@AddAwardCategories']);
        Route::get('/DeleteCategory', ['as' => 'backend.DeleteCategory', 'uses' => 'BackendController@DeleteCategory']);
        Route::post('/EditAwardCategories', ['as' => 'backend.EditAwardCategories', 'uses' => 'BackendController@EditAwardCategories']);

        Route::get('/AwardSubCategories', ['as' => 'backend.AwardSubCategories', 'uses' => 'BackendController@AwardSubCategories']);
        Route::post('/AddAwardSubCategories', ['as' => 'backend.AddAwardSubCategories', 'uses' => 'BackendController@AddAwardSubCategories']);
        Route::post('/EditAwardSubCategories', ['as' => 'backend.EditAwardSubCategories', 'uses' => 'BackendController@EditAwardSubCategories']);
        Route::get('/DeleteSubCategory', ['as' => 'backend.DeleteSubCategory', 'uses' => 'BackendController@DeleteSubCategory']);


        Route::get('/ListOfMembers', ['as' => 'backend.ListOfMembers', 'uses' => 'BackendController@ListOfMembers']);
        Route::get('/FilterMembers', ['as' => 'backend.FilterMembers', 'uses' => 'BackendController@FilterMembers']);
        Route::post('/FilterMembers_', ['as' => 'backend.FilterMembers_', 'uses' => 'BackendController@FilterMembers_']);
        Route::get('/ApproveMember', ['as' => 'backend.ApproveMember', 'uses' => 'BackendController@ApproveMember']);
        Route::get('/BusinessApproveMember', ['as' => 'backend.BusinessApproveMember', 'uses' => 'BackendController@BusinessApproveMember']);

        Route::get('/AddAttractions', ['as' => 'backend.AddAttractions', 'uses' => 'BackendController@AddAttractions']);
        Route::get('/AddNews', ['as' => 'backend.AddNews', 'uses' => 'BackendController@AddNews']);
        Route::post('/AddNews_', ['as' => 'backend.AddNews_', 'uses' => 'BackendController@AddNews_']);
        Route::get('/NewsList', ['as' => 'backend.NewsList', 'uses' => 'BackendController@NewsList']);
        Route::get('/EditNews', ['as' => 'backend.EditNews', 'uses' => 'BackendController@EditNews']);
        Route::post('/EditNews_', ['as' => 'backend.EditNews_', 'uses' => 'BackendController@EditNews_']);
        Route::get('/DeleteNews', ['as' => 'backend.DeleteNews', 'uses' => 'BackendController@DeleteNews']);
        Route::get('/GuidesList', ['as' => 'backend.GuidesRequest', 'uses' => 'BackendController@GuidesList']);
        Route::get('/BankSlip', ['as' => 'backend.BankSlip', 'uses' => 'BackendController@BankSlip']);
        Route::get('/AddBankSlip', ['as' => 'backend.AddBankSlip', 'uses' => 'BackendController@AddBankSlip']);
        Route::post('/BankSlip_', ['as' => 'backend.BankSlip_', 'uses' => 'BackendController@BankSlip_']);
        Route::post('/AddAttractions_', ['as' => 'backend.AddAttractions_', 'uses' => 'BackendController@AddAttractions_']);
        Route::get('/SendSMS', ['as' => 'backend.SendSMS', 'uses' => 'BackendController@SendSMS']);
        Route::post('/SendSMS_', ['as' => 'backend.SendSMS_', 'uses' => 'BackendController@SendSMS_']);
        Route::get('/SendEmail', ['as' => 'backend.SendEmail', 'uses' => 'BackendController@SendEmail']);
        Route::post('/SendEmail_', ['as' => 'backend.SendEmail_', 'uses' => 'BackendController@SendEmail_']);

        Route::get('/CompanyCategory', ['as' => 'backend.CompanyCategory', 'uses' => 'BackendController@CompanyCategory']);
        Route::post('/CompanyCategory_', ['as' => 'backend.CompanyCategory_', 'uses' => 'BackendController@CompanyCategory_']);
        Route::get('/EditCompanyCategory', ['as' => 'backend.EditCompanyCategory', 'uses' => 'BackendController@EditCompanyCategory']);
        Route::post('/EditCompanyCategory_', ['as' => 'backend.EditCompanyCategory_', 'uses' => 'BackendController@EditCompanyCategory_']);
        Route::get('/DeleteCompanycategory', ['as' => 'backend.DeleteCompanycategory', 'uses' => 'BackendController@DeleteCompanycategory']);
        Route::get('/AddSubCompanyCat', ['as' => 'backend.AddSubCompanyCat', 'uses' => 'BackendController@AddSubCompanyCat']);
        Route::post('/AddSubCompanyCat_', ['as' => 'backend.AddSubCompanyCat_', 'uses' => 'BackendController@AddSubCompanyCat_']);
        Route::get('/EditSubCompany', ['as' => 'backend.EditSubCompany', 'uses' => 'BackendController@EditSubCompany']);
        Route::post('/EditSubCompany_', ['as' => 'backend.EditSubCompany_', 'uses' => 'BackendController@EditSubCompany_']);
        Route::get('/DeleteSubCompany', ['as' => 'backend.DeleteSubCompany', 'uses' => 'BackendController@DeleteSubCompany']);
//        Route::post('/HotelCategory', ['as' => 'backend.HotelCategory', 'uses' => 'FrontendController@HotelCategory']);
//        Route::post('/SubCategory', ['as' => 'backend.SubCategory', 'uses' => 'FrontendController@SubCategory']);


    });
});





