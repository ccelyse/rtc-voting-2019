<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyCategory extends Model
{
    protected $table = "companycategory";
    protected $fillable = ['id','company_category'];
}
