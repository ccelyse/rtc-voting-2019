<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VotingNominee extends Model
{
    protected $table = "votingnominee";
    protected $fillable = ['id','votestatus','nominee_id','nominee_category','nominee_sms_code','voterphonenumber','voteremail','voternames'];
}
