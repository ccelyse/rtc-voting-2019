<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessAwards extends Model
{
    protected $table = "businessawards";
    protected $fillable = ['id','nomineepicture','nomineecompanyname','awardcategory','people_sub_award','experience','sender_name','sender_email','sender_phonenumber','nomineekey','nomineestatus'];

}
