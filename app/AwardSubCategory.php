<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AwardSubCategory extends Model
{
    protected $table = "award_subcategory";
    protected $fillable = ['id','award_category_id','award_sub_category'];
}
