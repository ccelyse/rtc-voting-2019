<?php

namespace App\Http\Controllers;

use AfricasTalking\SDK\AfricasTalking;
use App\Attractions;
use App\AwardCategory;
use App\AwardSubCategory;
use App\BusinessAwards;
use App\CompanyCategory;
use App\Countries;
use App\Hireaguide;
use App\JoinMember;
use App\PeopleAwards;
use App\Post;
use App\SubCompany;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;
require_once('./Class_MR_SMS_API.php');
class BackendController extends Controller
{
    public function SignIn_(){

    }
    public function AccountList(){
        $listaccount = User::all();
        return view('backend.AccountList')->with(['listaccount'=>$listaccount]);
    }
    public function Dashboard(){
        /*$now = Carbon::now();
        $nowdate = Carbon::now();
        $nowyear = $now->addYear(2);


        $day = strtotime($now->toDateString());

        $renewdate = JoinMember::whereBetween('expiration_date',[$nowdate,$nowyear])->get();

        foreach ($renewdate as $datas){
            $date_renew = $datas->expiration_date;
            $dateonly = strtotime($date_renew);
//            echo "<p>$dateonly</p>";
            $nowexp = time();
            $datediff = $dateonly - $nowexp;
            $daysexpiration = round($datediff / (60 * 60 * 24));

            if ($daysexpiration >= 30){

            }
        }*/


        $jan = new Carbon('first day of January');

        $jan_end = new Carbon('last day of January');

        $feb = new Carbon('first day of February');
        $feb_end = new Carbon('last day of February');

        $march = new Carbon('first day of March');
        $march_end = new Carbon('last day of March');

        $april = new Carbon('first day of April');
        $april_end = new Carbon('last day of April');

        $may= new Carbon('first day of May');
        $may_end = new Carbon('last day of May');

        $june = new Carbon('first day of June');
        $june_end = new Carbon('last day of June');

        $july = new Carbon('first day of July');
        $july_end = new Carbon('last day of July');

        $august = new Carbon('first day of August');
        $august_end = new Carbon('last day of August');

        $septmber = new Carbon('first day of September');
        $septmber_end = new Carbon('last day of September');

        $october = new Carbon('first day of October');
        $october_end = new Carbon('last day of October');

        $november = new Carbon('first day of November');
        $november_end = new Carbon('last day of November');

        $december = new Carbon('first day of December');
        $december_end = new Carbon('last day of December');


        /**
         *
         *  application member
         */

        $appmember = PeopleAwards::select(DB::raw('count(id) as appmember'))->value('appmember');
        $appmemberApproved = PeopleAwards::select(DB::raw('count(id) as appmember'))->where('nomineestatus','Approved')->value('appmember');
        $appmemberDenied = PeopleAwards::select(DB::raw('count(id) as appmember'))->where('nomineestatus','Denied')->value('appmember');
        $appmemberBussDenied = BusinessAwards::select(DB::raw('count(id) as appmember'))->where('nomineestatus','Denied')->value('appmember');
        $appmemberBussApproved = BusinessAwards::select(DB::raw('count(id) as appmember'))->where('nomineestatus','Approved')->value('appmember');
        $appmemberBuss = BusinessAwards::select(DB::raw('count(id) as appmember'))->value('appmember');

        $approvedmember = JoinMember::select(DB::raw('count(id) as approvedmember'))
            ->where('approval','Approved')
            ->value('approvedmember');

        $deniedmember = JoinMember::select(DB::raw('count(id) as deniedmember'))
            ->where('approval','Denied')
            ->value('deniedmember');

        $guiderequest = Hireaguide::select(DB::raw('count(id) as guiderequest'))->value('guiderequest');

        /**
         *
         * MEmber application  (JANUARY)
         */

        $janmember = JoinMember::select(DB::raw('count(id) as janmember'))
            ->whereBetween('created_at',[$jan->format('Y/m/d'),$jan_end->format('Y/m/d')])
            ->value('janmember');

        /**
         *
         * MEmber application  (Feb)
         */

        $febmember = JoinMember::select(DB::raw('count(id) as febmember'))
            ->whereBetween('created_at',[$feb->format('Y/m/d'),$feb_end->format('Y/m/d')])
            ->value('febmember');

        /**
         *
         * MEmber application  (mar)
         */

        $marmember = JoinMember::select(DB::raw('count(id) as marmember'))
            ->whereBetween('created_at',[$march->format('Y/m/d'),$march_end->format('Y/m/d')])
            ->value('marmember');
        /**
         *
         * MEmber application  (apr)
         */

        $aprmember = JoinMember::select(DB::raw('count(id) as aprmember'))
            ->whereBetween('created_at',[$april->format('Y/m/d'),$april_end->format('Y/m/d')])
            ->value('aprmember');

        /**
         *
         * MEmber application  (may)
         */

        $maymember = JoinMember::select(DB::raw('count(id) as maymember'))
            ->whereBetween('created_at',[$may->format('Y/m/d'),$may_end->format('Y/m/d')])
            ->value('maymember');

        /**
         *
         * MEmber application  (jun)
         */

        $junmember = JoinMember::select(DB::raw('count(id) as junmember'))
            ->whereBetween('created_at',[$june->format('Y/m/d'),$june_end->format('Y/m/d')])
            ->value('junmember');

        /**
         *
         * MEmber application  (jul)
         */

        $julmember = JoinMember::select(DB::raw('count(id) as julmember'))
            ->whereBetween('created_at',[$july->format('Y/m/d'),$july_end->format('Y/m/d')])
            ->value('julmember');

        /**
         *
         * MEmber application  (aug)
         */

        $augmember = JoinMember::select(DB::raw('count(id) as augmember'))
            ->whereBetween('created_at',[$august->format('Y/m/d'),$august_end->format('Y/m/d')])
            ->value('augmember');

        /**
         *
         * MEmber application  (sep)
         */

        $sepmember = JoinMember::select(DB::raw('count(id) as sepmember'))
            ->whereBetween('created_at',[$septmber->format('Y/m/d'),$septmber_end->format('Y/m/d')])
            ->value('sepmember');


        /**
         *
         * MEmber application  (oct)
         */


        $octmember = JoinMember::select(DB::raw('count(id) as octmember'))
            ->whereBetween('created_at',[$october->format('Y/m/d'),$october_end->format('Y/m/d')])
            ->value('octmember');

        /**
         *
         * MEmber application  (nov)
         */


        $novmember = JoinMember::select(DB::raw('count(id) as novmember'))
            ->whereBetween('created_at',[$november->format('Y/m/d'),$november_end->format('Y/m/d')])
            ->value('novmember');

        /**
         *
         * MEmber application  (dec)
         */

        $decmember = JoinMember::select(DB::raw('count(id) as decmember'))
            ->whereBetween('created_at',[$december->format('Y/m/d'),$december_end->format('Y/m/d')])
            ->value('decmember');

        return view('backend.Dashboard')->with(['appmemberBussDenied'=>$appmemberBussDenied,'appmemberDenied'=>$appmemberDenied,'appmemberBussApproved'=>$appmemberBussApproved,'appmemberApproved'=>$appmemberApproved,'appmemberBuss'=>$appmemberBuss,'janmember'=>$janmember,'febmember'=>$febmember,
            'marmember'=>$marmember,'aprmember'=>$aprmember,'maymember'=>$maymember,'junmember'=>$junmember,'julmember'=>$julmember,
            'augmember'=>$augmember,'sepmember'=>$sepmember,'octmember'=>$octmember,'novmember'=>$novmember,'decmember'=>$decmember,
            'appmember'=>$appmember,'approvedmember'=>$approvedmember,'deniedmember'=>$deniedmember,'guiderequest'=>$guiderequest]);
    }
    public function CreateAccount(){
        return view('backend.CreateAccount');
    }
    public function CreateAccount_(Request $request){
        $all  = $request->all();

        $request->validate([
            'name' => 'required|string:posts|max:255',
            'email' => 'required|string|email|max:255|unique:users','exists:connection.user,email',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $register = new User();
        $register->name = $request['name'];
        $register->email = $request['email'];
        $register->password = bcrypt($request['password']);
        $register->save();

        return redirect()->route('backend.AccountList')->with('Success','You have successfully Created Your Account');
    }
    public function ListofPeopleAward(){
//        $peopleaward = PeopleAwards::orderBy('created_at', 'desc')
//            ->join('awardcategory', 'awardcategory.id', '=', 'peopleawards.nomineecategory')
//            ->select('peopleawards.*','awardcategory.awardsubcategory')
//            ->get();

        $peopleaward = PeopleAwards::orderBy('created_at', 'desc')
            ->join('awardcategory', 'awardcategory.id', '=', 'peopleawards.nomineecategory')
            ->join('award_subcategory', 'award_subcategory.id', '=', 'peopleawards.people_sub_cate')
            ->select('peopleawards.*','awardcategory.awardsubcategory','award_subcategory.award_sub_category')
            ->get();
        $listcategory = AwardCategory::where('awardcategory','People Award')->get();
        return view('backend.ListofPeopleAward')->with(['listcategory'=>$listcategory,'peopleaward'=>$peopleaward]);
    }
    public function ListofPeopleAwardFilter(Request $request){
        $fromdate = $request['fromdate'];
        $todate = $request['todate'];
        $status =$request['status'];
//        $peopleaward = PeopleAwards::orderBy('created_at', 'desc')
//            ->whereBetween('peopleawards.created_at',[$fromdate,$todate])
//            ->where('peopleawards.nomineestatus',$status)
//            ->join('awardcategory', 'awardcategory.id', '=', 'peopleawards.nomineecategory')
//            ->select('peopleawards.*','awardcategory.awardsubcategory')
//            ->get();

        $peopleaward = PeopleAwards::orderBy('created_at', 'desc')
            ->whereBetween('peopleawards.created_at',[$fromdate,$todate])
            ->where('peopleawards.nomineestatus',$status)
            ->join('awardcategory', 'awardcategory.id', '=', 'peopleawards.nomineecategory')
            ->join('award_subcategory', 'award_subcategory.id', '=', 'peopleawards.people_sub_cate')
            ->select('peopleawards.*','awardcategory.awardsubcategory','award_subcategory.award_sub_category')
            ->get();
        $listcategory = AwardCategory::where('awardcategory','Business Award')->get();
        return view('backend.ListofPeopleAwardFilter')->with(['listcategory'=>$listcategory,'peopleaward'=>$peopleaward]);
    }
    public function EditListofPeopleAward(Request $request){
        $all = $request->all();
        $id = $request['id'];


        if($request->file('nomineepicture')){
            $updatenominee = PeopleAwards::find($id);
            $updatenominee->nameofnominee = $request['nameofnominee'];
            $updatenominee->establishment = $request['establishment'];
            $updatenominee->nomineecategory = $request['nomineecategory'];
            $updatenominee->experience = $request['experience'];
            $nomineepicture = $request->file('nomineepicture');
            $updatenominee->nomineepicture = time() .'_'. $request->file('nomineepicture')->getClientOriginalName();  ;
            $nomineepicture_ = time() .'_'. $request->file('nomineepicture')->getClientOriginalName();  ;
            $destinationPath = public_path('/peopleaward');
            $nomineepicture->move($destinationPath, $nomineepicture_);
            $updatenominee->save();
        }else{
            $updatenominee = PeopleAwards::find($id);
            $updatenominee->nameofnominee = $request['nameofnominee'];
            $updatenominee->establishment = $request['establishment'];
            $updatenominee->nomineecategory = $request['nomineecategory'];
            $updatenominee->experience = $request['experience'];
            $updatenominee->save();
        }
        return back()->with('success','you have successfully edited nominee information');
    }
    public function ListOfBusinessAward(){
        $peopleaward = BusinessAwards::orderBy('created_at', 'desc')
            ->join('awardcategory', 'awardcategory.id', '=', 'businessawards.awardcategory')
            ->join('award_subcategory', 'award_subcategory.id', '=', 'businessawards.people_sub_award')
            ->select('businessawards.*','awardcategory.awardsubcategory','award_subcategory.award_sub_category')
            ->get();
        $listcategory = AwardCategory::where('awardcategory','Business Award')->get();
        return view('backend.ListOfBusinessAward')->with(['listcategory'=>$listcategory,'peopleaward'=>$peopleaward]);
    }
    public function ListOfBusinessAwardFilter(Request $request){
        $fromdate = $request['fromdate'];
        $todate = $request['todate'];
        $status =$request['status'];
        $peopleaward = BusinessAwards::orderBy('created_at', 'desc')
            ->whereBetween('businessawards.created_at',[$fromdate,$todate])
            ->where('businessawards.nomineestatus',$status)
            ->join('awardcategory', 'awardcategory.id', '=', 'businessawards.awardcategory')
            ->join('award_subcategory', 'award_subcategory.id', '=', 'businessawards.people_sub_award')
            ->select('businessawards.*','awardcategory.awardsubcategory','award_subcategory.award_sub_category')
            ->get();
        $listcategory = AwardCategory::where('awardcategory','Business Award')->get();
        return view('backend.ListOfBusinessAward')->with(['listcategory'=>$listcategory,'peopleaward'=>$peopleaward]);
    }
    public function ListOfBusinessAwardEdit(Request $request){
        $all = $request->all();
        $id = $request['id'];


        if($request->file('nomineepicture')){
            $updatenominee = BusinessAwards::find($id);
            $updatenominee->nomineecompanyname = $request['nomineecompanyname'];
            $updatenominee->awardcategory = $request['awardcategory'];
            $updatenominee->experience = $request['experience'];
            $nomineepicture = $request->file('nomineepicture');
            $updatenominee->nomineepicture = time() .'_'. $request->file('nomineepicture')->getClientOriginalName();  ;
            $nomineepicture_ = time() .'_'. $request->file('nomineepicture')->getClientOriginalName();  ;
            $destinationPath = public_path('/businessaward');
            $nomineepicture->move($destinationPath, $nomineepicture_);
            $updatenominee->save();

        }else{

            $updatenominee = BusinessAwards::find($id);
            $updatenominee->nomineecompanyname = $request['nomineecompanyname'];
            $updatenominee->awardcategory = $request['awardcategory'];
            $updatenominee->experience = $request['experience'];
            $updatenominee->save();

        }

        return back()->with('success','you have successfully edited nominee information');
    }
    public function ListOfMembers(){
        $listmembers = JoinMember::orderBy('created_at', 'desc')->where('Memberstatus','No Status yet')->orWhere('Memberstatus', 'Approved')
            ->join('users', 'users.id', '=', 'JoinMember.user_id')
            ->select('JoinMember.*','users.name')
            ->get();
        return view('backend.ListOfMembers')->with(['listmembers'=>$listmembers]);

    }

    public function ApproveMember(Request $request){

        $id = $request['MemberId'];
        $status = $request['Status'];
        $currentMonth = Carbon::now()->format('m');
        $code = mt_rand(10, 99). $currentMonth;
        $date_now = Carbon::now();
        $dateafteryear = Carbon::now()->addYear();

        switch ($status) {
            case "Approved":
                $update_member = PeopleAwards::find($id);
                $update_member->nomineestatus ='Approved';
                $update_member->nomineekey =$code;
                $update_member->save();
                break;

            case "Denied":
                $update_member = PeopleAwards::find($id);
                $update_member->nomineestatus ='Denied';
                $update_member->nomineekey ='';
                $update_member->save();
                break;

        }
        return back()->with('success','You have successfully updated People award list');
    }

    public function BusinessApproveMember(Request $request){

        $id = $request['MemberId'];
        $status = $request['Status'];
        $currentMonth = Carbon::now()->format('m');
        $code = mt_rand(10, 99). $currentMonth;
        $date_now = Carbon::now();
        $dateafteryear = Carbon::now()->addYear();


        switch ($status) {
            case "Approved":
                $update_member = BusinessAwards::find($id);
                $update_member->nomineestatus ='Approved';
                $update_member->nomineekey =$code;
                $update_member->save();
                break;

            case "Denied":
                $update_member = BusinessAwards::find($id);
                $update_member->nomineestatus ='Denied';
                $update_member->nomineekey ='';
                $update_member->save();
                break;

        }
        return back()->with('success','You have successfully updated Business award list');
    }
    public function NotifyMember(Request $request){
            $all = $request->all();
            $rsgamessage = $request['limitedtextarea'];
            $phonenumber = $request['phonenumber'];
            $intnumber = '25'.$phonenumber;
            $api_key = 'aWhqaXU9Tk5zZmRJTD13PXJpckw=';
            $from = 'CHAMBER-PSF';
            $destination = $intnumber;
            $action='send-sms';
            $url = 'https://mistasms.com/sms/api';
            $sms = $rsgamessage;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.mista.io/sms',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('to' => $destination,'from' => 'RCOT','unicode' => '0','sms' => $sms,'action' => 'send-sms'),
                CURLOPT_HTTPHEADER => array(
                    'x-api-key: 12|S7455JX2BatRojzHKl24cU5DyFsPeAVefGMrK1Bc '
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $responsedata=json_decode($response);
//            echo $response;
//            dd($responsedata->status);
//        curl_close($curl);
            if($responsedata->status == 'success'){
                return back()->with('success','you have successfully sent your message');
            }else{
                return back()->with('danger','you have is not successfully sent');
            }

    }
    public function SendSMS_(Request $request){
//        $all = $request->all();
//        dd($all);
//        $username   = "rha";
//        $apiKey     = "55fa2436c4624ed856c4910111fde3ecd425a6bc738ed2fc20d6c85c438258b8";
        $message = $request['limitedtextarea'];
        $phonenumber = $request['phonenumber'];
        $pluscode = '+25';
        $recipients = $pluscode . $phonenumber;

        $username = 'sandbox'; // use 'sandbox' for development in the test environment
        $apiKey 	= 'fa1c39e0943a7802136e51e9b04acc43367003b517b09e6954463a688c27315f'; // use your sandbox app API key for development in the test environment
        $AT = new AfricasTalking($username, $apiKey,"sandbox");
        $SMS = $AT->sms();
        $options = array(
            "to" => '+250782384772',
            "message" => "$message",
        );

        $SMS->send($options);
        return redirect()->route('backend.SendSMS')->with('success','you have successfully sent your message');
    }
    public function AwardCategories(){
        $listcat = AwardCategory::all();
        return view('backend.AwardCategories')->with(['listcat'=>$listcat]);
    }

    public function AddAwardCategories(Request $request){
        $all = $request->all();
        $addcat = new AwardCategory();
        $addcat->awardcategory = $request['awardcategory'];
        $addcat->awardsubcategory = $request['awardsubcategory'];

        $image = $request->file('awardimage');
        $addcat->awardimage = $image->getClientOriginalName();
        $imagename = $image->getClientOriginalName();
        $destinationPath = public_path('/AwardImage');
        $image->move($destinationPath, $imagename);
        $addcat->save();

        return back()->with('success','you have successfully added new record');
    }
    public function EditAwardCategories(Request $request){
        $all = $request->all();

        if($image = $request->file('awardimage')){
            $id = $request['id'];
            $editcat = AwardCategory::find($id);
            $editcat->awardcategory = $request['awardcategory'];
            $editcat->awardsubcategory = $request['awardsubcategory'];
            $image = $request->file('awardimage');
            $editcat->awardimage = $image->getClientOriginalName();
            $imagename = $image->getClientOriginalName();
            $destinationPath = public_path('/AwardImage');
            $image->move($destinationPath, $imagename);
            $editcat->save();
        }else{
            $id = $request['id'];
            $editcat = AwardCategory::find($id);
            $editcat->awardcategory = $request['awardcategory'];
            $editcat->awardsubcategory = $request['awardsubcategory'];
            $editcat->save();
        }

        return back()->with('success','you have successfully edited new record');
    }
    public function DeleteCategory(Request $request){
        $id = $request['id'];
        $deletecat = AwardCategory::find($id);
        $deletecat->delete();
        return back()->with('success','you have successfully deleted award category');
    }
    public function AwardSubCategories(){
        $listcat = AwardCategory::all();
        $listsub = AwardSubCategory::join('awardcategory', 'awardcategory.id', '=', 'award_subcategory.award_category_id')
            ->select('awardcategory.awardsubcategory','awardcategory.awardcategory', 'award_subcategory.*')->get();;
        return view('backend.AwardSubCategories')->with(['listcat'=>$listcat,'listsub'=>$listsub]);
    }
    public function AddAwardSubCategories(Request $request){
        $all = $request->all();
        $addcat = new AwardSubCategory();
        $addcat->award_category_id = $request['award_category_id'];
        $addcat->award_sub_category = $request['award_sub_category'];
        $addcat->save();

        return back()->with('success','you have successfully added new record');
    }
    public function EditAwardSubCategories(Request $request){
        $id = $request['id'];
        $editcat = AwardSubCategory::find($id);
        $editcat->award_category_id = $request['award_category_id'];
        $editcat->award_sub_category = $request['award_sub_category'];
        $editcat->save();
        return back()->with('success','you have successfully edited record');
    }
    public function DeleteSubCategory(Request $request){
        $id = $request['id'];
        $deletecat = AwardSubCategory::find($id);
        $deletecat->delete();
        return back()->with('success','you have successfully deleted award sub category');
    }

}
