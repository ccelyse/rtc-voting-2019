<?php

namespace App\Http\Controllers;

use AfricasTalking\SDK\AfricasTalking;
use App\AwardCategory;
use App\AwardSubCategory;
use App\BusinessAwards;
use App\CompanyCategory;
use App\JoinMember;
use App\Mail\TestEmail;
use App\PeopleAwards;
use App\Post;
use App\SubCompany;
use App\VoteWithEmail;
use App\VotingNominee;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Midnite81\GeoLocation\Contracts\Services\GeoLocation;
use Stevebauman\Location\Location;
use Illuminate\Support\Facades\Mail;

require_once('./Class_MR_SMS_API.php');
class FrontendController extends Controller
{
    public function Home(){
        return view('welcome');
    }
    public function AboutUs(){
        $headertitle = 'Adopting Innovative Approaches to boost Intra-Africa travel as a drive for Tourism Business Recovery.';
        $para = '';
        return view('Aboutus')->with(['headertitle'=>$headertitle,'para'=>$para]);
    }
    public function BecomeMember(){
        $headertitle = 'OUR MEMBERS';
        $para = '';
        $listcompany = CompanyCategory::all();
        return view('BecomeMember')->with(['listcompany'=>$listcompany])->with(['headertitle'=>$headertitle,'para'=>$para]);
    }
    public function AboutTourismGala(){
        $headertitle = 'ABOUT RWANDA awards INDUSTRY GALA';
        $para = "Adopting Innovative Approaches to boost Intra-Africa travel as a drive for Tourism Business Recovery.";
        return view('AboutTourismGala')->with(['headertitle'=>$headertitle,'para'=>$para]);
    }
    public function Nominate(){
        $headertitle = 'NOMINATE NOW';
        $para = '';
        return view('Nominate')->with(['headertitle'=>$headertitle,'para'=>$para]);
    }
    public function AwardCategories(){
        $headertitle = 'Award categories';
        $para = '';
        return view('AwardCategories')->with(['headertitle'=>$headertitle,'para'=>$para]);
    }
    public function AwardSubCategory(Request $request){
        $headertitle = 'AWARD SUB CATEGORY';
        $para = '';
        $id = $request['id'];
        $cat = $request['cat'];
        $listawardcat = AwardSubCategory::where('award_category_id',$id)->get();
//        dd($listawardcat);
        return view('AwardSubCategory')->with(['cat'=>$cat,'listawardcat'=>$listawardcat,'headertitle'=>$headertitle,'para'=>$para]);

    }
    public function ApiSubCategory(Request $request){
        $id = $request['id'];
        $show = AwardSubCategory::where('award_category_id',$id)->get();
        if(0 == count($show)){
            return response()->json([
                'response_message' => "failed",
                'response_status' =>400
            ]);
        }else{
            return $show->toArray();
        }
    }
    public function NominateNow(){
        $headertitle = 'NOMINATE NOW';
        $para = '';
        $listpeoplecat = AwardCategory::where('awardcategory','People Award')->get();
        $listawardcat = AwardCategory::where('awardcategory','Business Award')->get();
        return view('NominateNow')->with(['para'=>$para,'listpeoplecat'=>$listpeoplecat,'listawardcat'=>$listawardcat,'headertitle'=>$headertitle]);
    }
    public function SendYourNominee(Request $request){
        $all = $request->all();

        $request->validate([
            'nomineepicture' => 'required|mimes:jpeg,jpg,png|max:2000',
        ]);

        $headertitle = 'Thank you for your submission';
        $para = '';
        $sender_name = $request['sender_name'];
        $nomineepicture = $request->file('nomineepicture');
        $savenominee = new PeopleAwards();
        $savenominee->nameofnominee = $request['nameofnominee'];
        $savenominee->establishment = $request['establishment'];
        $savenominee->nomineecategory = $request['nomineecategory'];
        $savenominee->people_sub_cate = $request['people_sub_cate'];
        $savenominee->experience = $request['experience'];
        $savenominee->sender_name = $request['sender_name'];
        $savenominee->sender_email = $request['sender_email'];
        $savenominee->sender_phonenumber = $request['sender_phonenumber'];
        $savenominee->nomineekey = '';
        $savenominee->nomineestatus = 'Not Yet Approved';

//        $savenominee->nomineepicture = $nomineepicture->getClientOriginalName();
        $savenominee->nomineepicture = time() .'_'. $request->file('nomineepicture')->getClientOriginalName();  ;
        $nomineepicture_ = time() .'_'. $request->file('nomineepicture')->getClientOriginalName();  ;
        $destinationPath = public_path('/peopleaward');
        $nomineepicture->move($destinationPath, $nomineepicture_);
        $savenominee->save();

        return view('Thankyou')->with(['sender_name'=>$sender_name,'headertitle'=>$headertitle,'para'=>$para]);
    }
    public function SendYourBusinessNominee(Request $request){
        $request->validate([
            'nomineepicture' => 'required|mimes:jpeg,jpg,png|max:2000',
        ]);

        $all = $request->all();
        $headertitle = 'Thank you for your submission';
        $para = '';
        $sender_name = $request['sender_name'];
        $nomineepicture = $request->file('nomineepicture');
        $savenominee = new BusinessAwards();
        $savenominee->nomineecompanyname = $request['nomineecompanyname'];
        $savenominee->awardcategory = $request['awardcategory'];
        $savenominee->people_sub_award = $request['people_sub_award'];
        $savenominee->experience = $request['experience'];
        $savenominee->sender_name = $request['sender_name'];
        $savenominee->sender_email = $request['sender_email'];
        $savenominee->sender_phonenumber = $request['sender_phonenumber'];
        $savenominee->nomineekey = '';
        $savenominee->nomineestatus = 'Not Yet Approved';

//        $savenominee->nomineepicture = $nomineepicture->getClientOriginalName();
//        $nomineepicture_ = $nomineepicture->getClientOriginalName();
//        $destinationPath = public_path('/businessaward');
//        $nomineepicture->move($destinationPath, $nomineepicture_);
//        $savenominee->save();
        $savenominee->nomineepicture = time() .'_'. $request->file('nomineepicture')->getClientOriginalName();  ;
        $nomineepicture_ = time() .'_'. $request->file('nomineepicture')->getClientOriginalName();  ;
        $destinationPath = public_path('/businessaward');
        $nomineepicture->move($destinationPath, $nomineepicture_);
        $savenominee->save();

        return view('Thankyou')->with(['para'=>$para,'headertitle'=>$headertitle,'sender_name'=>$sender_name]);
    }
    public function Google(){

        return \File::get(pulbic_path() . '/google259078c174a141f7.html');
    }
    public function SiteMap(){

        return \File::get(pulbic_path() . '/sitemap.xml');
    }
    public function VoteNow(){
        $headertitle = 'VOTE NOW';
        $para = '';
        return view('VoteNow')->with(['headertitle'=>$headertitle,'para'=>$para]);
    }
    public function BusinessAwardCategory(){
        $headertitle = 'AWARD CATEGORIES ';
        $para = '';
//        $listpeoplecat = AwardCategory::where('awardcategory','People Award')->get();
        $listpeoplecat = AwardCategory::where('awardcategory','Business Award')->get();
        return view('BusinessAwardCategory')->with(['listpeoplecat'=>$listpeoplecat,'headertitle'=>$headertitle,'para'=>$para]);

    }
    public function LocationUser()
    {

        $ipdf = request()->ip();
        $arr_ip = geoip()->getLocation($ipdf);
        $getlocation = $arr_ip->country;

    }
    public function PeopleAwardCategory(){
        $headertitle = 'PEOPLE AWARD CATEGORY';
        $para = '';
        $listpeoplecat = AwardCategory::where('awardcategory','People Award')->get();
        $listawardcat = AwardCategory::where('awardcategory','Business Award')->get();
        return view('PeopleAwardCategory')->with(['listpeoplecat'=>$listpeoplecat,'headertitle'=>$headertitle,'para'=>$para]);

    }
    public function AwardsCategory(Request $request){

        $all =$request->all();
        $id = $request['id'];
        $cat = $request['cat'];
//        dd($cat);
        $headertitle = 'VOTE NOW';
        $para = '';

        if($cat == 'People Award'){
            $picturepath = 'peopleaward';
            $nominees = PeopleAwards::where('people_sub_cate',$id)->where('nomineestatus','Approved')->get();
            foreach ($nominees as $data){
                $this->VotesNumber($data,$cat);
                $this->VotesNumberEmail($data,$cat);
                $data['VotesNumber'] = (double) $this->VotesNumber($data,$cat);
                $data['VotesNumberEmail'] = (double) $this->VotesNumberEmail($data,$cat);
            }
//            dd($nominees);

            return view('AwardsCategory')->with(['picturepath'=>$picturepath,'id'=>$id,'cat'=>$cat,'nominees'=>$nominees,'headertitle'=>$headertitle,'para'=>$para]);
        }else{
            $picturepath = 'businessaward';
            $nominees = BusinessAwards::where('people_sub_award',$id)->where('nomineestatus','Approved')->get();
            foreach ($nominees as $data){
                $this->VotesNumber($data,$cat);
                $this->VotesNumberEmail($data,$cat);
                $data['VotesNumber'] = (double) $this->VotesNumber($data,$cat);
                $data['VotesNumberEmail'] = (double) $this->VotesNumberEmail($data,$cat);
            }
            return view('AwardsCategory')->with(['picturepath'=>$picturepath,'id'=>$id,'cat'=>$cat,'nominees'=>$nominees,'headertitle'=>$headertitle,'para'=>$para]);
        }
    }

    public function VotesNumber($data,$cat){
        if($cat == 'People Award'){
            $votes = VotingNominee::select(DB::raw('count(id) as votes'))->where('nominee_id',$data->id)
                ->where('nominee_category',$data->people_sub_cate)
                ->where('votestatus','1')
                ->value('votes');
            return json_encode($votes);
        }else{
            $votes = VotingNominee::select(DB::raw('count(id) as votes'))->where('nominee_id',$data->id)
                ->where('nominee_category',$data->people_sub_award)
                ->where('votestatus','1')
                ->value('votes');
            return json_encode($votes);
        }

    }
    public function VotesNumberEmail($data,$cat){
        if($cat == 'People Award'){
            $votes = VoteWithEmail::select(DB::raw('count(id) as votes'))->where('nominee_id',$data->id)
                ->where('nominee_category',$data->people_sub_cate)
                ->where('votestatus','1')
                ->value('votes');
            return json_encode($votes);
        }else{
            $votes = VoteWithEmail::select(DB::raw('count(id) as votes'))->where('nominee_id',$data->id)
                ->where('nominee_category',$data->people_sub_award)
                ->where('votestatus','1')
                ->value('votes');
            return json_encode($votes);
        }

    }
    public function VoteYourNominee(Request $request){
        $all = $request->all();

        $id = $request['id'];
        $cat = $request['cat'];

        if($cat == 'People Award'){
            $nominee_category= PeopleAwards::where('id',$id)->value('people_sub_cate');

        }else{
            $nominee_category = BusinessAwards::where('id',$id)->value('people_sub_award');
        }
        $headertitle = 'VOTING REQUIREMENTS';
        $para = '';
        $ipdf = request()->ip();
        $arr_ip = geoip()->getLocation($ipdf);
        $getlocation = $arr_ip->country;
//        dd($getlocation);


        return view('VoteYourNominee')->with(['getlocation'=>$getlocation,'id'=>$id,'nominee_category'=>$nominee_category,'headertitle'=>$headertitle,'para'=>$para]);
    }
    public function SendVoterNumber(Request $request){
        $voterphonenumber = $request['voterphonenumber'];
//        $voterphonenumber = '0782384772';
        $currentMonth = Carbon::now()->format('m');
        $code = mt_rand(10, 9999);
        $nominee_id = $request['nominee_id'];
        $voteremail = $request['voteremail'];
        $voternames = $request['voternames'];
//        $nominee_id = '3';
        $nominee_category = $request['nominee_category'];
//        $nominee_category = '2';
        $nominee_sms_code = $code;
        $votestatus = '0';
        $voterphonenumber_ = $voterphonenumber;

        $checkphone = VotingNominee::where('nominee_id',$nominee_id)->where('voterphonenumber',$voterphonenumber)->value('voterphonenumber');

        $checkemail = VotingNominee::where('nominee_id',$nominee_id)->where('voteremail',$voteremail)->value('voteremail');

        if($checkphone == null AND $checkemail == null){
            $rsgamessage = 'Use '.$code.' to verify your phone number,  '.'Koresha iyi mibare '.$code.' mu kwerekana ko telephone yawe ariyo';
            $intnumber = '25'.$voterphonenumber;
            $destination = $intnumber;
            $sms = $rsgamessage;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.mista.io/sms',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('to' => $destination,'from' => 'RCOT','unicode' => '0','sms' => $sms,'action' => 'send-sms'),
                CURLOPT_HTTPHEADER => array(
                    'x-api-key: 12|S7455JX2BatRojzHKl24cU5DyFsPeAVefGMrK1Bc '
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $responsedata=json_decode($response);

            $savevoting = new VotingNominee();
            $savevoting->nominee_id = $nominee_id;
            $savevoting->nominee_category = $nominee_category;
            $savevoting->nominee_sms_code = $nominee_sms_code;
            $savevoting->voterphonenumber = $voterphonenumber_;
            $savevoting->voteremail = $voteremail;
            $savevoting->voternames = $voternames;
            $savevoting->votestatus = $votestatus;
            $savevoting->save();
            echo "We sent you a verification code to your phone number. Please type it below to continue.";

        }else{
            echo "Ooops, your phone number or email is has voted for this nominee.";
        }

    }
    public function SendVoterSmsCode(Request $request){
        $voterphonenumber = $request['voterphonenumber'];
        $nominee_id = $request['nominee_id'];
        $nominee_category = $request['nominee_category'];
        $smscode = $request['smscode'];
        $checksmscode = VotingNominee::where('voterphonenumber',$voterphonenumber)->where('nominee_id',$nominee_id)->where('nominee_category',$nominee_category)->value('nominee_sms_code');

        if($checksmscode == $smscode){

            $updatestatus = VotingNominee::where('voterphonenumber',$voterphonenumber)->where('nominee_id',$nominee_id)->where('nominee_category',$nominee_category)->update(['votestatus' => 1]);

            echo "You have successfully voted";

        }else{
            echo "Sorry your SMS code does not match";
        }

    }

    public function VoteWithEmail(Request $request){
        $VoteWithEmail = $request['voteremail'];
        $nominee_id = $request['nominee_id'];
        $nominee_category = $request['nominee_category'];
        $ipdf = request()->ip();
        $arr_ip = geoip()->getLocation($ipdf);

//        $VoteWithEmail = "kalisa913vdsvsddew@gmail.com";
//        $nominee_id = "19";
//        $nominee_category = "11";

        $checkemailexist = VoteWithEmail::where('voteremail',$VoteWithEmail)
            ->where('nominee_id',$nominee_id)
            ->where('nominee_category',$nominee_category)
            ->get();
        $checkipexist = VoteWithEmail::where('voter_ipaddress',$ipdf)
            ->where('nominee_id',$nominee_id)
            ->where('nominee_category',$nominee_category)
            ->get();

        if(0 == count($checkipexist)){
            if(0 == count($checkemailexist)){

                $savevoting = new VoteWithEmail();
                $getlocation = $arr_ip->country;
                $savevoting->nominee_id = $nominee_id;
                $savevoting->nominee_category = $nominee_category;
                $savevoting->voteremail = $VoteWithEmail;
                $savevoting->votestatus = "1";
                $savevoting->voter_ipaddress = $ipdf;
                $savevoting->voter_country = $getlocation;
                $savevoting->save();

                echo "You have successfully voted";

            }else{
                echo "Ooops, you have already voted for this nominee";
            }

        }else{
            echo "Ooops, you have already voted for this nominee";
        }

    }
    public function HotelCategory(Request $request)
    {
        $cate = $request['hotelcategory'];
//        $cate = 'Accomodation';
//        $cate = 'Bar';


        if ($cate == 'Accomodation') {
            $checksub = SubCompany::where('companycategory', $cate)->get();
//            echo "<option value=\"\" selected>Select Sub Category</option>";
//            echo "<label for=\"billing_country\" class=\"\">Company Sub Category</label><select class=\"country_to_state country_select billing_country\" name=\"hotelcategory\" id=\"hotelcategory\" required><option value=\"\">$datas->subcompanycategory</option></select>";
//            foreach ($checksub as $datas) {
//                echo "<option value=\"$datas->subcompanycategory\">$datas->subcompanycategory</option>";
//            }
            echo "<label for=\"billing_last_name\" class=\"\">Company Sub Category</label><select class=\"form-control\" id=\"basicSelect\" name=\"subcompanycategory\" style='width:60% !important' onChange=\"getSubCat(this.value);\"><option value=\"Hotel\">Hotel</option><option value=\"Apartment\">Apartment</option><option value=\"Villa\">Villa</option><option value=\"Motel\">Motel</option><option value=\"Cottage\">Cottage</option><option value=\"Logde\">Logde</option><option value=\"Tented camp\">Tented camp</option></select>";

        }elseif($cate == 'Restaurant' || $cate == 'Nightclub'|| $cate == 'Coffee shop' || $cate == 'Bar'){
//            echo "<option value=\"1 star\" selected>1 star</option>";
//            echo "<option value=\"2 star\" selected>2 star</option>";
//            echo "<option value=\"3 star\" selected>3 star</option>";
//            echo "<option value=\"4 star\" selected>4 star</option>";
//            echo "<option value=\"5 star\" selected>5 star</option>";
//
            echo "<label for=\"billing_last_name\" class=\"\">Seating Capacity (Eg:50)</label><input type=\"text\" class=\"input-text\" name=\"seatingcapacity\" id=\"billing_last_name\" placeholder=\"\"  required/>";
//            echo "<input type=\"text\" class=\"input-text\" name=\"Seatingcapacity\" id=\"billing_last_name\" placeholder=''>";
        }

    }
    public function SubCategory(Request $request){
        $subcompanycategory = $request['subcompanycategory'];
        if($subcompanycategory == 'Hotel' || $subcompanycategory == 'Apartment'|| $subcompanycategory == 'Logde'){
            echo "<label for=\"billing_last_name\" class=\"\">Grading</label><select class=\"form-control\" id=\"basicSelect\" name=\"subcompanycategory\" style='width:60% !important;margin-bottom: 15px;' onChange=\"getCity(this.value);\"><option value=\"Hotel\">1 star</option><option value=\"Apartment\">2 star</option><option value=\"Villa\">3 star</option><option value=\"Motel\">4 star</option><option value=\"Cottage\">5 star</option></select>";
            echo"<label for=\"billing_last_name\" class=\"\">Number of Rooms</label><input type=\"text\" class=\"input-text\" name=\"numberofrooms\" id=\"billing_last_name\" placeholder=\"\" />";
        }

    }

    public function JoinMembers(Request $request){
        $all = $request->all();

        $messages = [
            'fileToUpload' => 'The file to upload must be in PDF format',
        ];
        $request->validate([
            'fileToUpload'   => 'mimes:pdf'
        ],$messages);

        $companyname = $request['companyname'];
        $JoinM = new JoinMember();
        $Memberstatus = 'No Status yet';
        $image = $request->file('fileToUpload');

        $JoinM->typeofmembership = $request['typeofmembership'];
        $JoinM->chamber = $request['chamber'];
        $JoinM->companyname = $request['companyname'];
        $JoinM->companycode = $request['companycode'];
        $JoinM->hotelcategory = $request['hotelcategory'];
        $JoinM->numberofrooms = $request['numberofrooms'];
        $JoinM->gender = $request['gender'];
        $JoinM->phonenumber = $request['phonenumber'];
        $JoinM->pobx = $request['pobx'];
        $JoinM->email = $request['email'];
        $JoinM->website = $request['website'];
        $JoinM->businessaddress = $request['businessaddress'];
        $JoinM->buildingname = $request['buildingname'];
        $JoinM->businessarea = $request['businessarea'];
        $JoinM->province = $request['province'];
        $JoinM->district = $request['district'];
        $JoinM->sector = $request['sector'];
        $JoinM->cell = $request['cell'];
        $JoinM->companytype = $request['companytype'];
        $JoinM->ownership = $request['ownership'];
        $JoinM->businessactivity = $request['businessactivity'];
        $JoinM->export = $request['export'];
        $JoinM->numberofemployees = $request['numberofemployees'];
        $JoinM->numberofemployeesapart = $request['numberofemployeesapart'];
        $JoinM->Memberstatus = $Memberstatus;
        $JoinM->user_id = '1';
        $JoinM->documentname = $image->getClientOriginalName();

        $imagename = $image->getClientOriginalName();

        $input['fileToUpload'] = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/rdbcertificate');

        $image->move($destinationPath, $imagename);

        $JoinM->save();

        return view('Thankyou')->with(['companyname'=>$companyname]);
    }
    public function BusinessLicensing(){
        return view('BusinessLicensing');
    }
    public function News(){
        $newpost = Post::orderBy('created_at','desc')->get();
        return view('News')->with(['newpost'=>$newpost]);
    }
    public function NewsReadMore(Request $request){
        $id = $request['id'];
        $newpost = Post::where('id',$id)->get();
        return view('NewsReadMore')->with(['newpost'=>$newpost]);
    }
    public function Publications(){
        return view('Publications');
    }
    public function ContactUs(){
        $headertitle = 'CONTACT US';
        $para = '';
        return view('ContactUs')->with(['headertitle'=>$headertitle,'para'=>$para]);
    }
    public function TheProcess(){
        $headertitle = 'The Process';
        $para = '';
        return view('TheProcess')->with(['headertitle'=>$headertitle,'para'=>$para]);
    }
    public function Thankyou(){
        return view('Thankyou');
    }
    public function Login(){
        return view('backend.Login');
    }
    public function MemberDirectory(){
        $headertitle = 'OUR MEMBERS';
        $para = '';
        $listcat = JoinMember::where('approval','Approved')->get();
        return view('MemberDirectory')->with(['listcat'=>$listcat,'headertitle'=>$headertitle,'para'=>$para]);
    }
    public function Webmail(){
        return redirect('https://mail.rha.rw/');
    }
    public function TestMail(){
        $data = ['message' => 'This is a test!'];
        Mail::to('ccelyse1@gmail.com')->send(new TestEmail($data));

    }

}
