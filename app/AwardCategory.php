<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AwardCategory extends Model
{
    protected $table = "awardcategory";
    protected $fillable = ['id','awardcategory','awardsubcategory','awardimage'];
}
