<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VoteWithEmail extends Model
{
    protected $table = "votewithemail";
    protected $fillable = ['id','voter_ipaddress','voter_country','votestatus','nominee_id','nominee_category','voteremail'];
}
