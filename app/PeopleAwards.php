<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeopleAwards extends Model
{
    protected $table = "peopleawards";
    protected $fillable = ['id','nomineepicture','nameofnominee','establishment','nomineecategory','people_sub_cate','experience','sender_name','sender_email','sender_phonenumber','nomineekey','nomineestatus'];

}
