<?php
include 'db.php';
// escape variables for security

$typeofmembership= mysqli_real_escape_string($con, $_POST['typeofmembership']);
$chamber= mysqli_real_escape_string($con, $_POST['chamber']);
$companyname= mysqli_real_escape_string($con, $_POST['companyname']);
$companycode= mysqli_real_escape_string($con, $_POST['companycode']);
$hotelcategory= mysqli_real_escape_string($con, $_POST['hotelcategory']);
$numberofrooms= mysqli_real_escape_string($con, $_POST['numberofrooms']);
$gender= mysqli_real_escape_string($con, $_POST['gender']);
$phonenumber= mysqli_real_escape_string($con, $_POST['phonenumber']);
$pobx= mysqli_real_escape_string($con, $_POST['pobx']);
$email= mysqli_real_escape_string($con, $_POST['email']);
$website= mysqli_real_escape_string($con, $_POST['website']);
$businessaddress= mysqli_real_escape_string($con, $_POST['businessaddress']);
$buildingname= mysqli_real_escape_string($con, $_POST['buildingname']);
$businessarea= mysqli_real_escape_string($con, $_POST['businessarea']);
$province= mysqli_real_escape_string($con, $_POST['province']);
$district= mysqli_real_escape_string($con, $_POST['district']);
$sector= mysqli_real_escape_string($con, $_POST['sector']);
$cell= mysqli_real_escape_string($con, $_POST['cell']);
$companytype= mysqli_real_escape_string($con, $_POST['companytype']);
$ownership= mysqli_real_escape_string($con, $_POST['ownership']);
$businessactivity= mysqli_real_escape_string($con, $_POST['businessactivity']);
$export= mysqli_real_escape_string($con, $_POST['export']);
$numberofemployees= mysqli_real_escape_string($con, $_POST['numberofemployees']);
$numberofemployeesapart= mysqli_real_escape_string($con, $_POST['numberofemployeesapart']);
$now = date('Y-m-d H:i:s');

$currentDir = getcwd();
$uploadDirectory = "../rdbcertificate/";
$errors = []; // Store all foreseen and unforseen errors here
$fileExtensions = ['pdf']; // Get all the file extensions
$fileName = $_FILES['fileToUpload']['name'];
$fileSize = $_FILES['fileToUpload']['size'];
$fileTmpName  = $_FILES['fileToUpload']['tmp_name'];
$fileType = $_FILES['fileToUpload']['type'];
//$fileExtension = strtolower(end(explode('.',$fileName)));
//$uploadPath = $currentDir . $uploadDirectory . basename($fileName);

$sql = "INSERT INTO `members`(typeofmembership,chamber,companyname,companycode,
 hotelcategory,numberofrooms,gender,phonenumber,pobx,email,website,businessaddress,
  buildingname,businessarea,province,district,sector,cell,companytype,ownership,
   businessactivity,export,numberofemployees,numberofemployeesapart,created_at,documentname)VALUES('$typeofmembership',
   '$chamber','$companyname','$companycode','$hotelcategory','$numberofrooms','$gender',
    '$phonenumber','$pobx','$email','$website','$businessaddress','$buildingname','$businessarea',
    '$province','$district','$sector','$cell','$companytype','$ownership','$businessactivity','$export','$numberofemployees',
    '$numberofemployeesapart','$now','$fileName')";

move_uploaded_file($fileTmpName,$uploadDirectory.$fileName);

if (!mysqli_query($con,$sql)) {
    die('Error: ' . mysqli_error($con));
}

$string = '"Type of Membership","Chamber","Company Name","Company Code/Patent","Hotel Category","Number of Rooms","Gender","Phone number","P.O Box","Email Address","Website","Business Address Street Name","Building name","Business Area(Quartier)","Province","District","Sector","Cell","Company Type","Ownership","Business Activity","Export/Import Goods/Services","Number of Employees (Permanent)","Number of Employees (Part time)"' . PHP_EOL;
$string .= "\"$typeofmembership\",\"$chamber\",\"$companyname\",\"$companycode\",\"$hotelcategory\",\"$numberofrooms\",\"$gender\",\"$phonenumber\",\"$pobx\",\"$email\",\"$website\",\"$businessaddress\",\"$buildingname\",\"$businessarea\",\"$province\",\"$district\",\"$sector\",\"$cell\",\"$companytype\",\"$ownership\",\"$businessactivity\",\"$export\",\"$numberofemployees\",\"$numberofemployeesapart\"" . PHP_EOL;

file_put_contents('myfile.csv', $string);


//$last_id = $con->insert_id;

$random_hash = md5(date('r', time()));

$csvString = $string; // your entire csv as a string

$attachment = chunk_split(base64_encode($csvString));

$to = "rwandahospitalityassociation@gmail.com";
$from = $email;
$subject = "Members Application";
$body = "";

$headers = "From: $from\r\nReply-To: $from";
$headers .= "\r\nContent-Type: multipart/mixed; boundary=\"PHP-mixed-".$random_hash."\"";

$output = "
--PHP-mixed-$random_hash;
Content-Type: multipart/alternative; boundary='PHP-alt-$random_hash'
--PHP-alt-$random_hash
Content-Type: text/plain; charset='iso-8859-1'
Content-Transfer-Encoding: 7bit

$body

--PHP-mixed-$random_hash
Content-Type: text/csv; name=MemberApplication.csv
Content-Transfer-Encoding: base64
Content-Disposition: attachment

$attachment
--PHP-mixed-$random_hash--";

mail($to, $subject, $output, $headers);

//echo "Thank you for your account application, we will contact you shortly.";

header('location:../thankyou.php');

?>