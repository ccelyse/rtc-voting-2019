@extends('layouts.master')

@section('title', 'Our Members')

@section('content')

    {{--@include('layouts.SecTopMenu')--}}

    <style>
        header:before {
            content: '';
            position: absolute;
            background-image: url(assets/images/slider/20062978989_2ce115f88d_o.jpg) !important;
            background-size: cover;
            width: 100%;
            height: 100%;
        }
        header .page-title h1 {
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
        header .page-title p {
            color: #fff;
            font-size: 18px;
            line-height: 24px;
            width: 70%;
            margin: 0 auto;
        }
        .objective_list span {
            position: relative;
            bottom: 5px !important;
            color: #000;
        }
        .objective_list li {
            padding: 5px !important;
            margin-left: -20px;
        }
        a, * > a {
            color: #231f20;
            text-decoration: none;
            outline: none;
        }
        .page-title{
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
    </style>
    @include('layouts.topmenu')
    <div id="homeCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active" style="background:url('assets/images/slider/20062978989_2ce115f88d_o-min.jpg')">
                <div class="container">
                    <div class="carousel-caption">
                        <h1 class="page-title"><?php echo $headertitle?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <content>
        <div class="about" style="border-bottom:5px solid #231f20;">
            <div class="container">
                <div class="row about-top">
                    <div class="col-sm-12 top-space">
                        <p>The Chamber of Tourism now boasts major players in the industry in the various sub-sectors including Tourism information centers, hospitality service providers, tours and travel operators, tour guides, hospitality and tourism educators/trainers/schools.</p>


                        <h1 style="font-weight: bold">Currently the Chamber is made up of  5 professional associations</h1><br>

                        <ul class="objective_list">
                            <li> <span><a href="https://rha.rw/" target="_blank">Rwanda Hospitality Association (RHA)</a> </span></li>
                            <li><span><a href="https://rtta.rw/" target="_blank">Rwanda Tours and Travel Association (RTTA)</a></span></li>
                            <li><span><a href="https://rsga.rw/">Rwanda Safari Guides Association (RSGA)</a></span></li>
                            <li><span><a href="https://rhtea.rw/">Rwanda Hospitality and Tourism Educators Association (RHTEA)</a></span></li>
                            <li><span><a href="https://rata.org.rw/">Rwanda Travel Agencies Association (RATA).</a></span></li>
                            <li><span><a href="#">Rwanda Community-Based Tourism Initiatives</a></span></li>
                            <li><span><a href="#">Rwanda Professional Conference Organizers</a></span></li>
                        </ul><br>
{{--                        <h1 style="font-weight: bold">Upcoming Associations</h1><br>--}}
{{--                        <ul class="objective_list">--}}
{{--                            <li> <span>Rwanda Community Based Tourism Initiatives (RCBTI)</span></li>--}}
{{--                            <li><span>Rwanda Professional Conference Organizers (RPCO)</span></li>--}}
{{--                        </ul>--}}
                    </div>

                </div>

            </div>
        </div>
    </content>
    @include('layouts.footer')
@endsection