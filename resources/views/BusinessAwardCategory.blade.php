@extends('layouts.master')

@section('title', 'Business Award Category')

@section('content')

    {{--@include('layouts.SecTopMenu')--}}

    <style>
        header:before {
            content: '';
            position: absolute;
            background-image: url(assets/images/slider/20062978989_2ce115f88d_o.jpg) !important;
            background-size: cover;
            width: 100%;
            height: 100%;
        }
        header .page-title h1 {
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
        header .page-title p {
            color: #fff;
            font-size: 18px;
            line-height: 24px;
            width: 70%;
            margin: 0 auto;
        }
        .objective_list span {
            position: relative;
            bottom: 10px !important;
            color: #000;
        }
        .objective_list li {
            padding: 0px !important;
            margin-left: -20px;
        }
        a, * > a {
            color: #231f20;
            text-decoration: none;
            outline: none;
        }
        .page-title{
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
    </style>
    <content>
        @include('layouts.topmenu')
        <div id="homeCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="item active" style="background:url('assets/images/compressed/kcc3.jpeg')">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1 class="page-title"><?php echo $headertitle?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="about" style="border-bottom:5px solid #231f20;">
            <div class="container">
                <div class="row about-top" style="margin-bottom: 30px;">

                    @foreach($listpeoplecat as $cat)
                        <a href="{{ route('AwardSubCategory',['id'=> $cat->id,'cat'=>'Business Award'])}}">
                            <div class="col-lg-4 top-space" id="awardsvote">
                                <h1>{{$cat->awardsubcategory}}</h1>
                                <img src="AwardImage/{{$cat->awardimage}}" class="img-responsive">
                            </div>
                        </a>
                    @endforeach

                </div>
            </div>
        </div>
    </content>
    @include('layouts.footer')
@endsection