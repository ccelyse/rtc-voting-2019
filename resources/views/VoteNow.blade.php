@extends('layouts.master')

@section('title', 'Vote Now')

@section('content')

    {{--@include('layouts.SecTopMenu')--}}

    <style>
        header:before {
            content: '';
            position: absolute;
            background-image: url(assets/images/compressed/kcc1.jpeg) !important;
            background-size: cover;
            width: 100%;
            height: 100%;
        }
        header .page-title h1 {
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
        header .page-title p {
            color: #fff;
            font-size: 18px;
            line-height: 24px;
            width: 70%;
            margin: 0 auto;
        }
        .objective_list span {
            position: relative;
            bottom: 10px !important;
            color: #000;
        }
        .objective_list li {
            padding: 0px !important;
            margin-left: -20px;
        }
        a, * > a {
            color: #231f20;
            text-decoration: none;
            outline: none;
        }
        .page-title{
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
    </style>
    @include('layouts.topmenu')
    <content>
        <div id="homeCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="item active" style="background:url('assets/images/slider/DixdgguX4AAOsR3.jpg')">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1 class="page-title"><?php echo $headertitle?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="about" style="border-bottom:5px solid #231f20;">
            <div class="container">
                <div class="row about-top">
                    <a href="{{url('PeopleAwardCategory')}}">
                        <div class="col-lg-6 top-space" id="awardsvote">
                            <h1>People Awards</h1>
                            <img src="assets/images/compressed/d761ed_359dbc340258475688b2ed1c1d48a49a_mv2.jpg" class="img-responsive">
                        </div>
                    </a>
                   <a href="{{url('BusinessAwardCategory')}}">
                       <div class="col-lg-6 top-space" id="awardsvote">
                           <h1>Business Awards</h1>
                           <img src="assets/images/compressed/kcc2.jpeg" class="img-responsive">
                       </div>
                   </a>
                </div>

            </div>
        </div>
    </content>
    @include('layouts.footer')
@endsection