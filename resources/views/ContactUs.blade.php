@extends('layouts.master')

@section('title', 'CONTACT US')

@section('content')

{{--@include('layouts.SecTopMenu')--}}
<style>
    header:before {
        content: '';
        position: absolute;
        background-image: url(assets/images/slider/cytonn-photography-604668-unsplash-min.jpg) !important;
        background-size: cover;
        width: 100%;
        height: 100%;
    }
    header .page-title h1 {
        font-family: 'OpenSans';
        font-size: 36px;
        letter-spacing: 0.08em;
        color: #fff;
        text-transform: uppercase;
        font-weight: bold;
    }
    header .page-title p {
        color: #fff;
        font-size: 18px;
        line-height: 24px;
        width: 70%;
        margin: 0 auto;
    }
    .objective_list span {
        position: relative;
        bottom: 10px !important;
        color: #000;
    }
    .objective_list li {
        padding: 0px !important;
        margin-left: -20px;
    }
    a, * > a {
        color: #231f20;
        text-decoration: none;
        outline: none;
    }
    .page-title{
        font-family: 'OpenSans';
        font-size: 36px;
        letter-spacing: 0.08em;
        color: #fff;
        text-transform: uppercase;
        font-weight: bold;
    }
</style>
@include('layouts.topmenu')
<div id="homeCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        <div class="item active" style="background:url('assets/images/slider/cytonn-photography-604668-unsplash-min.jpg')">
            <div class="container">
                <div class="carousel-caption">
                    <h1 class="page-title"><?php echo $headertitle?></h1>
                </div>
            </div>
        </div>
    </div>
</div>
<content>
    <div class="about" style="border-bottom:5px solid #231f20;">
        <div class="container">
            <div class="row about-top">
                <div class="col-sm-6 top-space">
                    <p><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15949.999335745973!2d30.1035248!3d-1.9533694!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc76b5e00aaa31b51!2sM%26M+Plaza!5e0!3m2!1sen!2srw!4v1530612631578" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe></p>
                </div>
                <div class="col-sm-6 top-space" style="padding: 15px;">
                    <div class="col-md-8">
                        <h5>Office Contacts</h5>
                        <p>Call: 078841151/ 0788740465<br />
                            Email: <a href="" class="__cf_email__">info@rwandatourismchamber.org</a></p>
                    </div>

                    <div class="col-md-4">
                        <h5>Our Address</h5>
                        <p>Kigali,Rwanda<br />
                            Gishushu, KG 8 Avenue N.6<br />
                            M&M Plaza <br />
                            5th Floor</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</content>
@include('layouts.footer')
@endsection