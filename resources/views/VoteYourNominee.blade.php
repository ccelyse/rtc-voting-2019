@extends('layouts.master')

@section('title', 'VOTE NOW')

@section('content')

    @include('layouts.SecTopMenu')

    <style>
        header:before {
            content: '';
            position: absolute;
            background-image: url(assets/images/slider/20062978989_2ce115f88d_o.jpg) !important;
            background-size: cover;
            width: 100%;
            height: 100%;
        }
        header .page-title h1 {
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
        header .page-title p {
            color: #fff;
            font-size: 18px;
            line-height: 24px;
            width: 70%;
            margin: 0 auto;
        }
        .objective_list span {
            position: relative;
            bottom: 10px !important;
            color: #000;
        }
        .objective_list li {
            padding: 0px !important;
            margin-left: -20px;
        }
        a, * > a {
            color: #231f20;
            text-decoration: none;
            outline: none;
        }
        .winners .winners-list .winners-finalists .winners-finalists-list .winners-finalists-listpost, .past-winners .winners-list .winners-finalists .winners-finalists-list .winners-finalists-listpost{
            padding: 0px;
        }
        p {
            text-transform: none;
            letter-spacing: 0.05em;
            color: #000;
        }
        .modal .modal-dialog .modal-content {
            border-radius: 0;
            border: 2px solid #221f20;
            background: white;
            box-shadow: none;
        }
        .modal .modal-dialog .close {
            position: absolute;
            right: 0;
            top: 0;
            margin-top: -25px;
            text-shadow: none;
            color: #221f20;
            opacity: 1;
        }
    </style>
    <content>

        <div class="content winners" style="border-bottom: 5px solid #231f20;">
            <div class="container">
                <div class="winners-list">
                    <div class="row">
                            <div class="col-md-6 col-sm-offset-3" style="padding-top: 15px;">
                                <script type="text/javascript">

                                    $(document).on('click', '#submit', function() {
                                        var voterphonenumber = $('#voterphonenumber').val();
                                        var nominee_id = $('#nominee_id').val();
                                        var nominee_category = $('#nominee_category').val();
                                        var voternames = $('#voternames').val();
                                        var voteremail = $('#voteremail').val();

                                        function validateEmail(voteremail) {
                                            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                                            return re.test(voteremail);
                                        }

                                        if (validateEmail(voteremail)) {
                                            if(voternames){
                                            if (voterphonenumber.length > 10 || voterphonenumber.length < 10) {
                                                alert("Phone number has to be 10 characters!")
                                            }
                                            else{

                                                document.getElementById('send-code').style.display = 'block';
                                                document.getElementById('submit-code').style.display = 'block';
                                                document.getElementById('submit-phone').style.display = 'none';
                                                document.getElementById('submit').style.display = 'none';
                                                $.ajax({
                                                    headers: {
                                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                    },
                                                    type: "POST",
                                                    url: "SendVoterNumber",
                                                    data: {
                                                        'voterphonenumber': voterphonenumber,
                                                        'nominee_category': nominee_category,
                                                        'nominee_id': nominee_id,
                                                        'voternames': voternames,
                                                        'voteremail': voteremail
                                                    },
                                                    success: function (data) {
                                                        var response = data;
                                                        if (response == "We sent you a verification code to your phone number. Please type it below to continue.") {
                                                            document.getElementById("sent").style.display = "block";
                                                            document.getElementById("submit-voternames").style.display = "none";
                                                            document.getElementById("submit-voteremail").style.display = "none";
                                                        }
                                                        else if (response == "Sorry, your phone number or email  has voted for this nominee.") {
                                                            document.getElementById("exist").style.display = "block";
                                                            document.getElementById('send-code').style.display = 'none';
                                                            document.getElementById('submit-code').style.display = 'none';
                                                            document.getElementById("submit-voternames").style.display = "none";
                                                            document.getElementById("submit-voteremail").style.display = "none";
                                                        }

                                                    }
                                                });
                                            }

                                        }else{
                                            alert("Name is empty!")
                                        }
                                        } else {
                                            alert("Email is not valid");
                                        }

//

                                    });

                                    $(document).on('click', '#submit-code', function() {
                                        var voterphonenumber = $('#voterphonenumber').val();
                                        var nominee_id = $('#nominee_id').val();
                                        var nominee_category = $('#nominee_category').val();
                                        var smscode = $('#smscode').val();

                                        $.ajax({
                                            headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                            },
                                            type: "POST",
                                            url: "SendVoterSmsCode",
                                            data: {
                                                'voterphonenumber': voterphonenumber,
                                                'nominee_category': nominee_category,
                                                'nominee_id': nominee_id,
                                                'smscode': smscode

                                            },
                                            success: function (data) {
                                                var success = data;
                                                if (success == "You have successfully voted"){
                                                    document.getElementById('success').style.display = 'block';
                                                    document.getElementById('sent').style.display = 'none';
                                                    document.getElementById('send-code').style.display = 'none';
                                                    document.getElementById('submit-code').style.display = 'none';
                                                    document.getElementById('fail').style.display = 'none';
                                                }
                                                else if (success == "Sorry your SMS code does not match") {
                                                    document.getElementById('fail').style.display = 'block';
                                                }
                                            }
                                        });
                                    });


                                    //
                                </script>

                                <div class="pubnom-form-content ">

                                    <p><strong>We require you to use your phone number to vote</strong></p>
                                    <p>Here are the Steps:<br>
                                        1.Insert your phone number and click "SEND"<br>
                                        2.Check your SMS inbox for the confirmation code<br>
                                        3.Insert the confirmation code <br>
                                    </p>
                                    <p>Please follow the above steps to be able to submit your vote.</p>
                                    <p id="sent" style="font-weight: bold;color: #fba547; display: none;">We have sent to your phone number the verification code. To continue, please insert the code in the below box.</p>
                                    <p id="exist" style="font-weight: bold;color: #fba547; display: none;">Ooops, your phone number or email is has voted for this nominee.</p>
                                    <p id="success" style="font-weight: bold;color: #fba547; display: none;">You have successfully voted</p>
                                    <p id="fail" style="font-weight: bold;color: #fba547; display: none;">Sorry your SMS code does not match</p>

                                </div>
                                <div class="form-group" style="margin-left: 0px;" id="submit-phone">
                                    <label>Phone*</label>
                                    <div class="input input-textfield has-bender bender-topleft bender-border">
                                        <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                                               type = "number"  pattern="[+]{1}[0-9]{12}" maxlength="10" name="voterphonenumber" id="voterphonenumber" placeholder="0782384772" value="" data-validation="number" data-validation-error-msg="Please enter a valid phone number." required/>
                                        <span class="control-indicator"></span>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 0px;" id="submit-voternames">
                                    <label>Names</label>
                                    <div class="input input-textfield has-bender bender-topleft bender-border">
                                        <input type="text" id="voternames" name="voternames" placeholder="Names"/>
                                        <span class="control-indicator"></span>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 0px;" id="submit-voteremail">
                                    <label>Email</label>
                                    <div class="input input-textfield has-bender bender-topleft bender-border">
                                        <input type="text" id="voteremail" name="voteremail" placeholder="Email"/>
                                        <span class="control-indicator"></span>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 0px;" hidden>
                                    <label>Nominee  ID*</label>
                                    <div class="input input-textfield has-bender bender-topleft bender-border">
                                        <input type="text" id="nominee_id" name="nominee_id" value="<?php echo "$id";?>" readonly/>
                                        <span class="control-indicator"></span>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 0px; " hidden>
                                    <label>Nominee  Category*</label>
                                    <div class="input input-textfield has-bender bender-topleft bender-border">
                                        <input type="text" id="nominee_category" name="nominee_category" value="<?php echo "$nominee_category";?>" readonly />
                                    </div>
                                </div>
                                <div class="form-group" style="margin-left: 0px; display: none;" id="send-code">
                                    <label>SMS CODE*</label>
                                    <div class="input input-textfield has-bender bender-topleft bender-border">
                                        <input name="smscode"  id="smscode" type="number" placeholder="8765" value="" data-validation="number" data-validation-error-msg="Please enter a valid phone number." required/>
                                        <span class="control-indicator"></span>
                                    </div>
                                </div>
                                <a id="submit-code" class="button button-secondary has-bender" style="display: none;">Send code</a>
                                <a id="submit" class="button button-secondary has-bender">VOTE</a>
                            </div>
                </div>
            </div>
        </div>
        </div>
        </div>

    </content>
    @include('layouts.footer')
@endsection