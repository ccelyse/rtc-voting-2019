@extends('layouts.master')

@section('title', 'Chamber of Tourism')

@section('content')

    {{--@include('layouts.SecTopMenu')--}}
    <style>
        header:before {
            content: '';
            position: absolute;
            background-image: url(assets/images/benjaminrobyn-jespersen-540757-unsplash-min.jpg) !important;
            background-size: cover;
            width: 100%;
            height: 100%;
        }
        header .page-title h1 {
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
        header .page-title p {
            color: #fff;
            font-size: 18px;
            line-height: 24px;
            width: 70%;
            margin: 0 auto;
        }
        .page-title{
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
    </style>
    @include('layouts.topmenu')
    <div id="homeCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active" style="background:url('assets/images/benjaminrobyn-jespersen-540757-unsplash-min.jpg')">
                <div class="container">
                    <div class="carousel-caption">
                        <h1 class="page-title"><?php echo $headertitle?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <content>
        <div class="about" style="border-bottom:5px solid #231f20;">
            <div class="container">
                <div class="row about-top">
                    <div class="col-sm-12 top-space">
                        <p>Dear <strong><?php echo "$sender_name" ?></strong>,<br>Thank you for your submission. Our team will work on your application and get back to you as soon as possible.</p>
                    </div>
                </div>
            </div>
        </div>
    </content>
    @include('layouts.footer')
@endsection