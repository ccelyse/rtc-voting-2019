@extends('layouts.master')

@section('title', 'RWANDA TOURISM INDUSTRY GALA')

@section('content')
<style>
    .spb-asset-content p{
        color:#fff !important;
    }
    .title-wrap h3{
        color: #fff !important;
    }
    .header-top a{
        padding: 13px;
    }
    .home-sponsers img {
        background: #fff;
        /*width: 100;*/
        height: 140px;
        padding: 15px;
    }
</style>
@include('layouts.topmenu')

<div id="homeCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#homeCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#homeCarousel" data-slide-to="1" class=""></li>
        <li data-target="#homeCarousel" data-slide-to="2" class=""></li>
        <li data-target="#homeCarousel" data-slide-to="3" class=""></li>
        <li data-target="#homeCarousel" data-slide-to="4" class=""></li>
        {{--<li data-target="#homeCarousel" data-slide-to="5" class=""></li>--}}
        {{--<li data-target="#homeCarousel" data-slide-to="6" class=""></li>--}}
    </ol>

    <div class="carousel-inner" role="listbox">
        <div class="item active" style="background:url('assets/images/kingfisher-min.jpeg')">
            <div class="container">
                <div class="carousel-caption">
                    <div class="header-action">
                        <div class="header-nominate header-top">
                            <a href="{{url('AwardCategory')}}" class="button button-primary">
                                Vote Now
                            </a>
                            <a href="{{url('Nominate')}}" class="button button-primary">
                                Nominate Now
                            </a>
                        </div>
                    </div>
                    <!--<h1>LET THE STARS GUIDE YOU</h1>-->
                </div>
            </div>
        </div>
{{--        <div class="item" style="background:url('assets/images/Tea-min.jpeg')">--}}
{{--            <div class="container">--}}
{{--                <div class="carousel-caption">--}}
{{--                    <div class="header-action">--}}
{{--                        <div class="header-nominate header-top">--}}
{{--                            <a href="{{url('VoteNow')}}" class="button button-primary">--}}
{{--                                Vote Now--}}
{{--                            </a>--}}
{{--                            <a href="{{url('Nominate')}}" class="button button-primary">--}}
{{--                                Nominate Now--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!--<h1>LET THE STARS GUIDE YOU</h1>-->--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="item " style="background:url('assets/images/Question-Coffee-Gishushu-Kigali-Rwanda-6-min.jpg')">--}}
{{--            <div class="container">--}}
{{--                <div class="carousel-caption">--}}
{{--                    <div class="header-action">--}}
{{--                        <div class="header-nominate header-top">--}}
{{--                            <a href="{{url('VoteNow')}}" class="button button-primary">--}}
{{--                                Vote Now--}}
{{--                            </a>--}}
{{--                            <a href="{{url('Nominate')}}" class="button button-primary">--}}
{{--                                Nominate Now--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!--<h1>Rwanda Tourism Awards</h1>-->--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

        <div class="item " style="background:url('assets/images/7-min.jpg')">
            <div class="container">
                <div class="carousel-caption">
                    <div class="header-action">
                        <div class="header-nominate header-top">
                            <a href="{{url('VoteNow')}}" class="button button-primary">
                                Vote Now
                            </a>
                            <a href="{{url('Nominate')}}" class="button button-primary">
                                Nominate Now
                            </a>
                        </div>
                    </div>
                    <!--<h1>2018 Recipients Announced</h1>-->
                </div>
            </div>
        </div>

        <div class="item " style="background:url('assets/images/nyungwe-min.jpeg')">
            <div class="container">
                <div class="carousel-caption">
                    <div class="header-action">
                        <div class="header-nominate header-top">
                            <a href="{{url('VoteNow')}}" class="button button-primary">
                                Vote Now
                            </a>
                            <a href="{{url('Nominate')}}" class="button button-primary">
                                Nominate Now
                            </a>
                        </div>
                    </div>
                    <!--<h1>2018 Recipients Announced</h1>-->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.carousel -->
<content>

    <div class="content home" id="nominatepart">
        <div class="container">
            <div class="row content-title">
                <div class="col-lg-12">
                    <h2>How to Nominate?</h2>
                </div>
            </div>
            <div class="row home-nominate">
                <div class="col-lg-4">
                    <div class="steps">
                        <p class="steps-number"><i class="fas fa-trophy"></i></p>
                        <div class="steps-content  ">
                            <h1>Award Categories</h1>
                            <!--<p>Award Categories</p>-->
                            <p class="link-container"><a href="{{url('MoreAwardCategories')}}"><i class="fa fa-angle-right" aria-hidden="true"></i>Learn about the criteria for each award</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="steps">
                        <p class="steps-number"><i class="fas fa-badge-check"></i></p>
                        <div class="steps-content  ">
                            <h1>NOMINATE</h1>
                            <!--<p>Tell us why the individual or establishment deserves to win.</p>-->
                            <p class="link-container"><a href="{{url('Nominate')}}"><i class="fa fa-angle-right" aria-hidden="true"></i>Tell us why the individual or establishment deserves to win.</a></p>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="steps">
                        <p class="steps-number"><i class="fas fa-share-square"></i></p>
                        <div class="steps-content last ">
                            <h1>Vote and Share</h1>
                            <p class="link-container"><a href="{{url('VoteNow')}}"><i class="fa fa-angle-right" aria-hidden="true"></i>Support the nominees by giving your vote here</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="top-divider"></div>

    <div class="content home" id="sponsers">
        <div class="container">
            <div class="row content-title">
                <div class="col-lg-12">
                    <h2 style="color: #231f20;">Members</h2>
                </div>
            </div>
            <div class="row home-sponsers">

                <div class="col-lg-2">
                    <a href="https://rha.rw/" target="_blank"> <img src="assets/images/RHAlogo_vspkcp.png" class="img-responsive"></a>
                </div>
                <div class="col-lg-2" >
                    <a href="https://rtta.rw/" target="_blank"><img src="assets/images/RTTA_irfof0.png" class="img-responsive"></a>
                </div>
                <div class="col-lg-2" >
                    <a href="https://rsga.rw/" target="_blank"><img src="assets/images/RSGAlogo_udmmrj.png" class="img-responsive"></a>
                </div>
                <div class="col-lg-2" >
                    <a href="https://rhtea.rw/" target="_blank"><img src="assets/images/RHTEALOGO_lwsqgb.png" class="img-responsive"></a>
                </div>
                <div class="col-lg-2" >
                    <a href="https://rata.org.rw/" target="_blank"><img src="assets/images/rata-min_cqpumk.png" class="img-responsive"></a>
                </div>
                <div class="col-lg-2" >
                    <a href="#" target="_blank"><img src="assets/images/rita2-min_ltrtd1.png" class="img-responsive"></a>
                </div>
            </div>
        </div>
        <div class="container">
            {{--<div class="row content-title">--}}
                {{--<div class="col-lg-12">--}}
                    {{--<h2 style="color: #231f20;">Sponsors and Partners</h2>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row home-sponsers">--}}

                {{--<!--<div class="col-sm-2">-->--}}
                {{--<!--<img src="assets/images/logo.jpg">-->--}}
                {{--<!--</div>-->--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="https://www.marriott.com/hotels/fact-sheet/travel/kglmc-kigali-marriott-hotel/" target="_blank"><img src="assets/images/p8.jpg" class="img-responsive" alt="Marroit rwanda"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="https://www.oneandonlyresorts.com/"><img src="assets/images/p15.jpg" class="img-responsive" alt="oneandonlyresorts" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="https://www.visitrwanda.com/"><img src="assets/images/p17.jpg" class="img-responsive" alt="Visit Rwanda" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="https://www.turkishairlines.com/"><img src="assets/images/10.jpg" class="img-responsive" alt="Turkish airline" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="https://www.rwandair.com/"><img src="assets/images/p16.jpg" class="img-responsive" alt="Rwanda Air" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="http://www.akageraaviation.com/"><img src="assets/images/p4.jpg" class="img-responsive" alt="akagera aviation" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="http://primatesafaris.info/"><img src="assets/images/p6.jpg" class="img-responsive" alt="primate safaris" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="https://www.thousandhillsafrica.com/"><img src="assets/images/p7.jpg" class="img-responsive" alt="thousand hills africa" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="https://www.uberluxesafaris.com/"><img src="assets/images/p13.jpg" class="img-responsive" alt="uberluxesafaris" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="http://nyungwehotel.com/"><img src="assets/images/9.jpg" class="img-responsive" alt="Nyungwe hotel" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="http://www.palasthotelrwanda.com/"><img src="assets/images/p10.jpg" class="img-responsive" alt="Palast rock hotel" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="https://www.serenahotels.com/serenakigali/en/default.html"><img src="assets/images/p20.jpg" class="img-responsive" alt="Serena Hotels Kigali" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="#"><img src="assets/images/p14.jpg" class="img-responsive" alt="ubumwe grande hotel" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="http://sd.rw/"><img src="assets/images/p2.jpg" class="img-responsive" alt="skyline digital" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="https://bluflamingo.digital/"><img src="assets/images/p1.jpg" class="img-responsive" alt="Blueflamingo" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="https://prime.rw/"><img src="assets/images/p22.jpg" class="img-responsive" alt="Prime insurance" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="https://www.psf.org.rw/"><img src="assets/images/p3.jpg" class="img-responsive" alt="psf" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="http://www.fivevolcanoesrwanda.com/"><img src="assets/images/p19.jpg" class="img-responsive" alt="five volcanoes rwanda" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="http://www.rwandaevents.com/"><img src="assets/images/p12.jpg" class="img-responsive" alt="rwanda events" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="http://www.startravels.rw/"><img src="assets/images/p11.jpg" class="img-responsive" alt="skyline digital" target="_blank"></a>--}}
                {{--</div>--}}
                {{--<div class="col-lg-2">--}}
                    {{--<a href="http://abgafrica.com/"><img src="assets/images/p21.jpg" class="img-responsive" alt="akagera business group" target="_blank"></a>--}}
                {{--</div>--}}



            {{--</div>--}}
        </div>
    </div>
</content>

@include('layouts.footer')
@endsection
