@extends('backend.layout.master')

@section('title', 'RHA')

@section('content')
    <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-col="2-columns">
    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        .btn-primary:hover{
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
    </style>
    {{--<script--}}
    {{--src="https://code.jquery.com/jquery-3.3.1.min.js"--}}
    {{--integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="--}}
    {{--crossorigin="anonymous"></script>--}}

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <div class="content-body">
                    <!-- Basic Summernote start -->
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <h4 class="card-title" id="basic-layout-form">Member info</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('EditJoinMember_') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            @foreach($edit as $data)
                                            <div class="form-body">
                                                <div class="row">
                                                    <input type="text" id="projectinput1" class="form-control" value="{{ $data->id }}"
                                                           name="id" required hidden>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Type of Membership</label>
                                                            <select class="form-control"  id="basicSelect" name="typeofmembership" required>
                                                                <option value="{{$data->typeofmembership}}">{{$data->typeofmembership}}</option>
                                                                <option value="Association" selected>Association</option>
                                                                <option value="Champions (Indashyikirwa)">Champions (Indashyikirwa)</option>
                                                                <option value="District">District</option>
                                                                <option value="Golden Circle">Golden Circle</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Chamber</label>
                                                            <select class="form-control"  id="basicSelect" name="chamber" required>
                                                                <option value="{{$data->chamber}}">{{$data->chamber}}</option>
                                                                <option value="Chamber of Tourism" selected>Chamber of Tourism</option>
                                                                <option value="ChamberofAgricultureandlivestock">Chamber of Agriculture and livestock</option>
                                                                <option value="Chamber of Arts Crafts and Artisans">Chamber of Arts Crafts and Artisans</option>
                                                                <option value="Chamber of Commerce and Services">Chamber of Commerce and Services</option>
                                                                <option value="Chamber of Financial Institutions">Chamber of Financial Institutions</option>
                                                                <option value="Chamber of ICT">Chamber of ICT</option>
                                                                <option value="Chamber of Industry">Chamber of Industry</option>
                                                                <option value="Chamber of Liberal Profession">Chamber of Liberal Profession</option>

                                                                <option value="Chamber of Women Entrepreneurs">Chamber of Women Entrepreneurs</option>
                                                                <option value="Chamber of Young Entrepreneurs">Chamber of Young Entrepreneurs</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Company Name</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ $data->companyname }}"
                                                                   name="companyname" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput2">Company Code/Tin number</label>
                                                            <input type="text" id="projectinput2" class="form-control" value="{{ $data->companycode }}" name="companycode" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Hotel/Restaurant/Bar/Nightclub Category</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ $data->hotelcategory }}"
                                                                   name="hotelcategory" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput2">Number of Rooms</label>
                                                            <input type="text" id="projectinput2" class="form-control" value="{{ $data->numberofrooms }}"
                                                                   name="numberofrooms" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Gender</label>
                                                            <select class="form-control"  id="basicSelect" name="gender" required>
                                                                <option value="{{$data->gender}}">{{$data->gender}}</option>
                                                                <option value="Male">Male</option>
                                                                <option value="Female">Female</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput2">Phone number</label>
                                                            <input type="text" id="projectinput2" class="form-control" value="{{ $data->phonenumber }}"
                                                                   name="phonenumber" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">P.O Box</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ $data->pobx }}"
                                                                   name="pobx">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput2">Email Address</label>
                                                            <input type="text" id="projectinput2" class="form-control" value="{{ $data->email }}"
                                                                   name="email" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Website</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ $data->website }}"
                                                                   name="website">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput2">Business Address Street Name</label>
                                                            <input type="text" id="projectinput2" class="form-control" value="{{ $data->businessaddress }}"
                                                                   name="businessaddress" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Building name</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ $data->buildingname }}"
                                                                   name="buildingname" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Business Area(Quartier)</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ $data->businessarea }}"
                                                                   name="businessarea" required>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Province</label>
                                                            <select class="form-control"  id="basicSelect" name="province" required>
                                                                <option value="{{$data->province}}">{{$data->province}}</option>
                                                                <option value="City of Kigali">City of Kigali</option>
                                                                <option value="Eastern Province">Eastern Province</option>
                                                                <option value="Western Province">Western Province</option>
                                                                <option value="Southern Province">Southern Province</option>
                                                                <option value="Northern Province">Northern Province</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">District</label>
                                                            <select class="form-control"  id="basicSelect" name="district" required>
                                                                <option value="{{$data->district}}">{{$data->district}}</option>
                                                                <option value="Bugesera">Bugesera</option>
                                                                <option value="Burera">Burera</option>
                                                                <option value="Gakenke">Gakenke</option>
                                                                <option value="Gasabo">Gasabo</option>
                                                                <option value="Gatsibo">Gatsibo</option>
                                                                <option value="Gicumbi">Gicumbi</option>
                                                                <option value="Gisagara">Gisagara</option>
                                                                <option value="Huye">Huye</option>
                                                                <option value="Kamonyi">Kamonyi</option>
                                                                <option value="Karongi">Karongi</option>
                                                                <option value="Kayonza">Kayonza</option>
                                                                <option value="Kicukiro">Kicukiro</option>
                                                                <option value="Kirehe">Kirehe</option>
                                                                <option value="Muhanga">Muhanga</option>
                                                                <option value="Musanze">Musanze</option>
                                                                <option value="Ngoma">Ngoma</option>
                                                                <option value="Ngororero">Ngororero</option>
                                                                <option value="Nyabihu">Nyabihu</option>
                                                                <option value="Nyagatare">Nyagatare</option>
                                                                <option value="Nyamagabe">Nyamagabe</option>
                                                                <option value="Nyamasheke">Nyamasheke</option>
                                                                <option value="Nyanza">Nyanza</option>
                                                                <option value="Nyarugenge">Nyarugenge</option>
                                                                <option value="Nyaruguru">Nyaruguru</option>
                                                                <option value="Rubavu">Rubavu</option>
                                                                <option value="Ruhango">Ruhango</option>
                                                                <option value="Rulindo">Rulindo</option>
                                                                <option value="Rusuzi">Rusuzi</option>
                                                                <option value="Rutsiro">Rutsiro</option>
                                                                <option value="Rwamagana">Rwamagana</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Sector</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ $data->sector }}"
                                                                   name="sector" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Cell</label>
                                                            <input type="text" id="projectinput1" class="form-control" value="{{ $data->cell }}"
                                                                   name="cell" required>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Ownership</label>
                                                            <select class="form-control"  id="basicSelect" name="ownership" required>
                                                                <option value="{{$data->ownership}}">{{$data->ownership}}</option>
                                                                <option value="100% foreign-owned">100% foreign-owned</option>
                                                                <option value="100% rwandan-owned">100% rwandan-owned</option>
                                                                <option value="Franchise">Franchise</option>
                                                                <option value="Joint venture (Foreign & Rwandan)">Joint venture (Foreign & Rwandan)</option>
                                                                <option value="Representation of a foreign company">Representation of a foreign company</option>
                                                                <option value="Other">Other</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Company Type</label>
                                                            <select class="form-control"  id="basicSelect" name="companytype" required>
                                                                <option value="{{$data->companytype}}">{{$data->companytype}}</option>
                                                                <option value="Cooperative">Cooperative</option>
                                                                <option value="NGO">NGO</option>
                                                                <option value="Partly state-owned company">Partly state-owned company</option>
                                                                <option value="Private Company">Private Company</option>
                                                                <option value="Public Company">Public Company</option>
                                                                <option value="Sole Trader">Sole Trader</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Business Activity</label>
                                                            <select class="form-control"  id="basicSelect" name="businessactivity" required>
                                                                <option value="{{$data->businessactivity}}">{{$data->businessactivity}}</option>
                                                                <option value="Accounting Services">Accounting Services</option>
                                                                <option value="Adventure Tours">Adventure Tours</option>
                                                                <option value="Advertising Services">Advertising Services</option>
                                                                <option value="Agriculture Inputs Dealers">Agriculture Inputs Dealers</option>
                                                                <option value="Agro-Processing">Agro-Processing</option>
                                                                <option value="Architect">Architect</option>
                                                                <option value="Auto Driving School">Auto Driving School</option>
                                                                <option value="Bank services">Bank services</option>
                                                                <option value="Bank services">Bank services</option>
                                                                <option value="Beauty Makers">Beauty Makers</option>
                                                                <option value="Beverage producer">Beverage producer</option>
                                                                <option value="Beverage producer">Beverage producer</option>
                                                                <option value="Broadcasting">Broadcasting</option>
                                                                <option value="Business Service">Business Service</option>
                                                                <option value="Business-Plan Consulting">Business-Plan Consulting</option>
                                                                <option value="Car Rental Agency">Car Rental Agency</option>
                                                                <option value="Catering Services">Catering Services</option>
                                                                <option value="Cleaning service">Cleaning service</option>
                                                                <option value="Construction service">Construction service</option>
                                                                <option value="Consultancy services">Consultancy services</option>
                                                                <option value="Contractor">Contractor</option>
                                                                <option value="Creative industry">Creative industry</option>
                                                                <option value="Decoration">Decoration</option>
                                                                <option value="Dry Cleaning & Laundry Services">Dry Cleaning & Laundry Services</option>
                                                                <option value="E-commerce‎">E-commerce‎</option>
                                                                <option value="Education">Education</option>
                                                                <option value="Electrical Services">Electrical Services</option>
                                                                <option value="Energy">Energy</option>
                                                                <option value="Engineering service">Engineering service</option>
                                                                <option value="Event Services">Event Services</option>
                                                                <option value="Farming">Farming</option>
                                                                <option value="Fashion industry‎ ">Fashion industry‎ </option>
                                                                <option value="Financial services‎">Financial services‎</option>
                                                                <option value="Fitness Center">Fitness Center</option>
                                                                <option value="Forex Bureaus">Forex Bureaus</option>
                                                                <option value="Freight Forwarders">Freight Forwarders</option>
                                                                <option value="Freight transport">Freight transport</option>
                                                                <option value="Garage services">Garage services</option>
                                                                <option value="Hair Salon">Hair Salon</option>
                                                                <option value="Health Services">Health Services</option>
                                                                <option value="Hospitality services">Hospitality services</option>
                                                                <option value="Insurance Broker">Insurance Broker</option>
                                                                <option value="Insurance service">Insurance service</option>
                                                                <option value="Internet providers">Internet providers</option>
                                                                <option value="Language Translation">Language Translation</option>
                                                                <option value="Leather workers">Leather workers</option>
                                                                <option value="Manufacturing">Manufacturing</option>
                                                                <option value="Marketing services">Marketing services</option>
                                                                <option value="Media industry‎">Media industry‎</option>
                                                                <option value="Metal Workers">Metal Workers</option>
                                                                <option value="Mining">Mining</option>
                                                                <option value="Online services‎">Online services‎</option>
                                                                <option value="Paint and coatings industry‎">Paint and coatings industry‎</option>
                                                                <option value="Petroleum transport">Petroleum transport</option>
                                                                <option value="Photography Services">Photography Services</option>
                                                                <option value="Practice of law‎ ">Practice of law‎ </option>
                                                                <option value="Printing Services">Printing Services</option>
                                                                <option value="Processing">Processing</option>
                                                                <option value="Professional services">Professional services</option>
                                                                <option value="Property Valuers service">Property Valuers service</option>
                                                                <option value="Public Auditor">Public Auditor</option>
                                                                <option value="Public transport">Public transport</option>
                                                                <option value="Real estate and construction">Real estate and construction</option>
                                                                <option value="Real Estate Brokers ">Real Estate Brokers </option>
                                                                <option value="Repair Services">Repair Services</option>
                                                                <option value="Restaurant Delivery Service">Restaurant Delivery Service</option>
                                                                <option value="Retailer">Retailer</option>
                                                                <option value="Safari Guide">Safari Guide</option>
                                                                <option value="Security Services">Security Services</option>
                                                                <option value="Seller">Seller</option>
                                                                <option value="Sewing and Tailoring">Sewing and Tailoring</option>
                                                                <option value="Shipping Services">Shipping Services</option>
                                                                <option value="Software Applications Developer">Software Applications Developer</option>
                                                                <option value="Tax Services">Tax Services</option>
                                                                <option value="Tour Operator">Tour Operator</option>
                                                                <option value="Tourism‎">Tourism‎</option>
                                                                <option value="Transport‎">Transport‎</option>
                                                                <option value="Travel Agencies">Travel Agencies</option>
                                                                <option value="Waste management">Waste management</option>
                                                                <option value="Web-Site Services">Web-Site Services</option>
                                                                <option value="Wholesaler">Wholesaler</option>
                                                                <option value="Wood worker">Wood worker</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Export/Import Goods/Services</label>
                                                            <select class="form-control"  id="basicSelect" name="export" >
                                                                <option value="{{$data->export}}">{{$data->export}}</option>
                                                                <option value="beverage">beverage</option>
                                                                <option value="coffee">coffee</option>
                                                                <option value="cars">cars</option>
                                                                <option value="electronic equipment">electronic equipment</option>
                                                                <option value="food products">food products</option>
                                                                <option value="gas">gas</option>
                                                                <option value="hardware">hardware</option>
                                                                <option value="horticulture">horticulture</option>
                                                                <option value="oil">oil</option>
                                                                <option value="pharmaceutical products">pharmaceutical products</option>
                                                                <option value="services">services</option>
                                                                <option value="spare parts">spare parts</option>
                                                                <option value="stationary">stationary</option>
                                                                <option value="Tea">Tea</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Number of Employees (Permanent)</label>
                                                            <select class="form-control"  id="basicSelect" name="numberofemployees" required>
                                                                <option value="{{$data->numberofemployees}}">{{$data->numberofemployees}}</option>
                                                                <option value="0">0</option>
                                                                <option value="From 1 to 5">From 1 to 5</option>
                                                                <option value="From 6  to 10">From 6  to 10</option>
                                                                <option value="From 11 to 20">From 11 to 20</option>
                                                                <option value="From 21 to 50">From 21 to 50</option>
                                                                <option value="From 51 to 100">From 51 to 100</option>
                                                                <option value="From 101 to 200">From 101 to 200</option>
                                                                <option value="From 201 to 500">From 201 to 500</option>
                                                                <option value="From 501 to 1000">From 501 to 1000</option>
                                                                <option value="More than 1000">More than 1000</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Number of Employees (Part time)</label>
                                                            <select class="form-control"  id="basicSelect" name="numberofemployeesapart" required>
                                                                <option value="{{$data->numberofemployeesapart}}">{{$data->numberofemployeesapart}}</option>
                                                                <option value="0">0</option>
                                                                <option value="From 1 to 5">From 1 to 5</option>
                                                                <option value="From 6  to 10">From 6  to 10</option>
                                                                <option value="From 11 to 20">From 11 to 20</option>
                                                                <option value="From 21 to 50">From 21 to 50</option>
                                                                <option value="From 51 to 100">From 51 to 100</option>
                                                                <option value="From 101 to 200">From 101 to 200</option>
                                                                <option value="From 201 to 500">From 201 to 500</option>
                                                                <option value="From 501 to 1000">From 501 to 1000</option>
                                                                <option value="More than 1000">More than 1000</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Attach your company registration certificate issued by RDB (only PDF is accepted)</label>
                                                            <input type="file" name="fileToUpload">
                                                        </div>
                                                    </div>
                                                </div>

                                                <button type="submit" class="btn btn-primary"> <i class="la la-check-square-o"></i> Save</button>
                                            </div>
                                            @endforeach

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Summernote Click to edit end -->
                    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
                    <script src="backend/app-assets/vendors/js/editors/summernote/summernote.js" type="text/javascript"></script>
                    <script src="backend/app-assets/js/scripts/editors/editor-summernote.min.js" type="text/javascript"></script>
                </div>
            </div>
        </div>
    </div>

@endsection
