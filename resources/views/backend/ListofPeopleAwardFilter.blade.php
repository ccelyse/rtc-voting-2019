@extends('backend.layout.master')

@section('title', 'RWANDA TOURISM INDUSTRY')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        #ui-datepicker-div{
            padding: 10px; table-responsive;
            background:#6b442b;
        }
        .ui-datepicker-prev,.ui-datepicker-next,.ui-datepicker-calendar{
            color: #fff !important;
            padding: 10px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="backend/app-assets/vendors/css/forms/selects/select2.min.css">
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
    <script>
    </script>

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body"><!-- HTML5 export buttons table -->
                <section id="html5">
                    <div class="row">
                        <form
                                method="POST"
                                action="{{ url('ListofPeopleAwardFilter') }}"
                                enctype="multipart/form-data" style="display: contents;">

                            {{ csrf_field() }}
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="date">Status</label>
                                    <select class="form-control" name="status">
                                        <option value="Approved">Approved</option>
                                        <option value="Not Yet Approved">Not Yet Approved</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="date">From Date</label>
                                    <input type="date" class="form-control" id="date" name="fromdate"
                                           required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="date">To Date</label>
                                    <input type="date" class="form-control" id="date" name="todate"
                                           required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" style="margin-top: 25px">
                                    <button type="submit" class="btn btn-login">
                                        <i class="la la-check-square-o"></i> Filter
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="col-12">
                            <div class="card">

                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered dataex-html5-export table-responsive">
                                            <thead>
                                            <tr>
                                                {{--<th>Product Image</th>--}}
                                                <th>Edit</th>
                                                <th>Send Message</th>
                                                <th>NOMINEE'S NAME</th>
                                                <th>ESTABLISHMENT</th>
                                                <th>Category</th>
                                                <th>View Experience</th>
                                                <th>View Nominee Picture</th>
                                                <th>Sender NAME</th>
                                                <th>Sender EMAIL</th>
                                                <th>Sender Phone</th>
                                                <th>Status</th>
                                                <th>Identification</th>
                                                <th>Approval</th>
                                                <th>Date Created</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($peopleaward as $data)
                                                <tr>
                                                    {{--<td><img src="images/{{$data->image}}" class="img-responsive" style="width: 100px"></td>--}}
                                                    <td>

                                                        <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                                data-toggle="modal"
                                                                data-target="#editnomie{{$data->id}}">Edit
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="editnomie{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1">Edit</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="form-horizontal form-simple" method="POST"
                                                                              action="{{ url('EditListofPeopleAward') }}"
                                                                              enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <div class="row  multi-field">

                                                                                <input type="text" name="id" value="{{$data->id}}" hidden>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Nominee's Name</label>
                                                                                        <input type="text" value="{{$data->nameofnominee}}" name="nameofnominee" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">ESTABLISHMENT</label>
                                                                                        <input type="text" value="{{$data->establishment}}" name="establishment" class="form-control">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">category</label>
                                                                                        <select class="form-control" name="nomineecategory">
                                                                                            <option value="{{$data->nomineecategory}}">{{$data->awardsubcategory}}</option>
                                                                                            @foreach($listcategory as $cate)
                                                                                                <option value="{{$cate->id}}">{{$cate->awardsubcategory}}</option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Experience</label>
                                                                                        <textarea class="form-control" name="experience" rows="10">{{$data->experience}}</textarea>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Save
                                                                                    </button>
                                                                                </div>

                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                                data-toggle="modal"
                                                                data-target="#sms{{$data->id}}">Send Nominee Code
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="sms{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1">Send Nominee Code</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="form-horizontal form-simple" method="POST"
                                                                              action="{{ url('NotifyMember') }}"
                                                                              enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <div class="row  multi-field">

                                                                                <div class="col-md-12">
                                                                                    <script language="javascript" type="text/javascript">
                                                                                        function limitText(limitField, limitCount, limitNum) {
                                                                                            if (limitField.value.length > limitNum) {
                                                                                                limitField.value = limitField.value.substring(0, limitNum);
                                                                                            } else {
                                                                                                limitCount.value = limitNum - limitField.value.length;
                                                                                            }
                                                                                        }
                                                                                    </script>

                                                                                    <textarea class='form-control' name="limitedtextarea" onKeyDown="limitText(this.form.limitedtextarea,this.form.countdown,160);"
                                                                                              onKeyUp="limitText(this.form.limitedtextarea,this.form.countdown,160);" rows="5" required>Dear {{$data->sender_name}} your application has been rejected</textarea><br>
                                                                                    <font size="1">(Maximum characters: 160)<br>
                                                                                </div>

                                                                                You have <input readonly type="text" name="countdown" size="3" value="160"> characters left.</font>

                                                                                <div class="col-md-12">
                                                                                    <div class="form-group">
                                                                                        <label for="projectinput1">Experience</label>
                                                                                        <input type="text" class="form-control" name="phonenumber" value="{{$data->sender_phonenumber}} ">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Send
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>{{$data->nameofnominee}}</td>
                                                    <td>{{$data->establishment}}</td>
                                                    <td>{{$data->awardsubcategory}}</td>
                                                    <td>

                                                        <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                                data-toggle="modal"
                                                                data-target="#appointtodoctors{{$data->id}}">View Experience
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="appointtodoctors{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1">Experience</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="row  multi-field">

                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">Experience</label>
                                                                                    <p>{{$data->experience}}</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>

                                                        <button type="button" class="btn btn-login btn-min-width mr-1 mb-1"
                                                                data-toggle="modal"
                                                                data-target="#picture{{$data->id}}">View Nominee Picture
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="picture{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1">View Nominee Picture</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="row  multi-field">

                                                                            <div class="col-md-12">
                                                                                <div class="form-group">
                                                                                    <label for="projectinput1">View Nominee Picture</label>
                                                                                    <p><img src="peopleaward/{{$data->nomineepicture}}" style="width: 100%;"></p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>{{$data->sender_name}}</td>
                                                    <td>{{$data->sender_email}}</td>
                                                    <td>{{$data->sender_phonenumber}}</td>
                                                    <td>{{$data->nomineekey}}</td>
                                                    <td>{{$data->nomineestatus}}</td>
                                                    <td>
                                                        <div class="form-group">
                                                            <!-- Outline Icon Button group -->
                                                            <p style="text-align: center">{{$data->approval}}</p>
                                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                                <a href="{{ route('backend.ApproveMember',['MemberId'=> $data->id,'Status'=> 'Approved'])}}" class="btn btn-icon btn-outline-secondary"><i class="fas fa-user-check"></i></a>
                                                                <a href="{{ route('backend.ApproveMember',['MemberId'=> $data->id,'Status'=> 'Denied'])}}" class="btn btn-icon btn-outline-primary"><i class="fas fa-user-times"></i></a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>{{$data->created_at}}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/jszip.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/pdfmake.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/vfs_fonts.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.html5.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.print.min.js"></script>
    <script src="backend/app-assets/vendors/js/tables/buttons.colVis.min.js"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables-extensions/datatable-button/datatable-html5.js"></script>


@endsection
