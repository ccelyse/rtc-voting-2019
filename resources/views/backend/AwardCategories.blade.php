@extends('backend.layout.master')

@section('title', 'RWANDA TOURISM INDUSTRY')

@section('content')

    @include('backend.layout.sidemenu')
    @include('backend.layout.upmenu')
    <style>
        .btn-secondary{
            color:#fff !important;
            background-color: #6a442b !important;
            border-color:#6a442b !important;
        }
        #ui-datepicker-div{
            padding: 10px; table-responsive;
            background:#6b442b;
        }
        .ui-datepicker-prev,.ui-datepicker-next,.ui-datepicker-calendar{
            color: #fff !important;
            padding: 10px;
        }
    </style>

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <section id="basic-form-layouts">
                    <div class="row match-height">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    @if (session('success'))
                                        <div class="alert alert-success" id="success_messages" style="margin-top: 10px;">
                                            {{ session('success') }}
                                        </div>
                                    @endif
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>

                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('AddAwardCategories') }}" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Award category</label>
                                                                <select class="form-control"  id="basicSelect" name="awardcategory" required>
                                                                    <option value="People Award">People Award</option>
                                                                    <option value="Business Award">Business Award</option>
                                                                </select>
                                                            </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="projectinput1">Award sub category</label>
                                                                <input type="text" class="form-control" name="awardsubcategory" required>
                                                            </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Award image</label>
                                                            <input type="file" class="form-control" name="awardimage" required>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="form-actions">
                                                    <button type="submit" class="btn btn-login">
                                                        <i class="la la-check-square-o"></i> Save
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
                <section id="complex-header">

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Award Category</h4>
                                    {{--@if (session('success'))--}}
                                        {{--<div class="alert alert-success" id="success_messages" style="margin-top: 10px;">--}}
                                            {{--{{ session('success') }}--}}
                                        {{--</div>--}}
                                    {{--@endif--}}

                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration table-responsive">
                                            <thead>
                                            <tr>
                                                <th>Category Name</th>
                                                <th>Sub Category Name</th>
                                                <th>Date Created</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($listcat as $data)
                                                <tr>
                                                    <td>{{$data->awardcategory}}</td>
                                                    <td>{{$data->awardsubcategory}}</td>
                                                    <td>{{$data->created_at}}</td>
                                                    <td>
                                                        <button type="button" class="btn btn-icon btn-outline-primary"
                                                                data-toggle="modal"
                                                                data-target="#category{{$data->id}}">Edit
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade text-left" id="category{{$data->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="myModalLabel1"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel1">Edit Information</h4>
                                                                        <button type="button" class="close" data-dismiss="modal"
                                                                                aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <form class="form-horizontal form-simple" method="POST"
                                                                              action="{{url('EditAwardCategories')}}"
                                                                              enctype="multipart/form-data">
                                                                            {{ csrf_field() }}
                                                                            <div class="row  multi-field">

                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Award Category</label>
                                                                                            <select class="form-control"  id="basicSelect" name="awardcategory" required>
                                                                                                <option value="{{$data->awardcategory}}">{{$data->awardcategory}}</option>
                                                                                                <option value="People Award">People Award</option>
                                                                                                <option value="Business Award">Business Award</option>
                                                                                            </select>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Award Sub Category</label>
                                                                                            <input type="text" class="form-control" name="awardsubcategory" value="{{$data->awardsubcategory}}" required>
                                                                                            <input type="text" class="form-control" name="id" value="{{$data->id}}" hidden>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label for="projectinput1">Award image</label>
                                                                                            <input type="file" class="form-control" name="awardimage">
                                                                                        </div>
                                                                                    </div>
                                                                                <div class="col-md-12" style="margin-bottom: 15px">
                                                                                    <img src="AwardImage/{{$data->awardimage}}" style="width: 100%">
                                                                                </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <button type="submit" class="btn btn-login">
                                                                                        <i class="la la-check-square-o"></i> Send
                                                                                    </button>
                                                                                </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><a href="{{ route('backend.DeleteCategory',['id'=> $data->id])}}" class="btn btn-icon btn-outline-primary">Delete</a></td>

                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
            </div>
        </div>
    </div>
    <script src="backend/app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/ui/jquery-ui/date-pickers.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.date.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/picker.time.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/pickadate/legacy.js" type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/pickers/daterange/daterangepicker.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"
            type="text/javascript"></script>
    <script src="backend/app-assets/vendors/js/tables/datatable/datatables.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app-menu.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/core/app.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/customizer.min.js" type="text/javascript"></script>
    <script src="backend/app-assets/js/scripts/tables/datatables/datatable-basic.js"
            type="text/javascript"></script>

@endsection
