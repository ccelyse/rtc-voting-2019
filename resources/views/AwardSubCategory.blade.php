@extends('layouts.master')

@section('title', 'AWARD SUB CATEGORY')

@section('content')

    {{--@include('layouts.SecTopMenu')--}}

    <style>
        header:before {
            content: '';
            position: absolute;
            background-image: url(assets/images/slider/votenow.jpg) !important;
            background-size: cover;
            width: 100%;
            height: 100%;
        }
        header .page-title h1 {
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
        header .page-title p {
            color: #fff;
            font-size: 18px;
            line-height: 24px;
            width: 70%;
            margin: 0 auto;
        }
        .objective_list span {
            position: relative;
            bottom: 10px !important;
            color: #000;
        }
        .objective_list li {
            padding: 0px !important;
            margin-left: -20px;
        }
        a, * > a {
            color: #231f20;
            text-decoration: none;
            outline: none;
        }
        .page-title{
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
    </style>
    @include('layouts.topmenu')
    <div id="homeCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active" style="background:url('/AwardImage/DkAHewlWsAA89Z7.jpg')">
                <div class="container">
                    <div class="carousel-caption">
                        <h1 class="page-title"><?php echo $headertitle?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <content>
        <content>
            <div class="about" style="border-bottom:5px solid #231f20;">
                <div class="container">
                    <div class="row about-top">

                        {{--@foreach($listpeoplecat as $cat)--}}
                            {{--<a href="{{ route('AwardsCategory',['id'=> $cat->id])}}">--}}
                                {{--<div class="col-lg-4 top-space" id="awardsvote">--}}
                                    {{--<h1>{{$cat->awardsubcategory}}</h1>--}}
                                    {{--<img src="AwardImage/{{$cat->awardimage}}" class="img-responsive">--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--@endforeach--}}

                        @if($cat == "Business Award")
                        @foreach($listawardcat as $cat)
                                <a href="{{ route('AwardsCategory',['id'=> $cat->id,'cat'=>'Business Award'])}}">
                                    <div class="col-lg-6" >
                                        <div id="awardsvotecate">

                                            <?php
                                            $appmember = \App\BusinessAwards::select(DB::raw('count(id) as appmember'))
                                                ->where('people_sub_award',$cat->id)
                                                ->where('nomineestatus','Approved')
                                                ->value('appmember');
                                            ?>

                                            <p>{{$cat->award_sub_category}} <span style="font-size: 16px;color: #fba547;"><strong>(<?php echo"$appmember"; ?>)</strong></span></p>
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                            @else
                            @foreach($listawardcat as $cat)
                                <a href="{{ route('AwardsCategory',['id'=> $cat->id,'cat'=>'People Award'])}}">
                                    <div class="col-lg-6" >
                                        <div id="awardsvotecate">
                                            <?php
                                            $appmember = \App\PeopleAwards::select(DB::raw('count(id) as appmember'))
                                                ->where('people_sub_cate',$cat->id)
                                                ->where('nomineestatus','Approved')
                                                ->value('appmember');
                                            ?>

                                            <p>{{$cat->award_sub_category}} <span style="font-size: 16px;color: #fba547;"><strong>(<?php echo"$appmember"; ?>)</strong></span></p>
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                             @endif



                        {{--<a href="{{url('BusinessAwardCategory')}}">--}}
                            {{--<div class="col-lg-6 top-space" id="awardsvote">--}}
                                {{--<h1>Business Awards</h1>--}}
                                {{--<img src="assets/images/slider/onomo3.jpg" class="img-responsive">--}}
                            {{--</div>--}}
                        {{--</a>--}}
                    </div>

                </div>
            </div>
        </content>

    </content>
    @include('layouts.footer')
@endsection