@extends('layouts.master')

@section('title', 'NOMINATE')

@section('content')

    {{--@include('layouts.SecTopMenu')--}}
    <style>
        header:before {
            content: '';
            position: absolute;
            background-image: url(assets/images/slider/benjaminrobyn-jespersen-540757-unsplash.jpg) !important;
            background-size: cover;
            width: 100%;
            height: 100%;
        }
        header .page-title h1 {
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
        header .page-title p {
            color: #fff;
            font-size: 18px;
            line-height: 24px;
            width: 70%;
            margin: 0 auto;
        }
        .objective_list span {
            position: relative;
            bottom: 5px !important;
            color: #000;
        }
        .objective_list li {
            padding: 0px !important;
            margin-left: -20px;
        }
        a, * > a {
            color: #231f20;
            text-decoration: none;
            outline: none;
        }
        .page-title{
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
    </style>
    @include('layouts.topmenu')
    <div id="homeCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
{{--            <div class="item active" style="background:url('assets/images/slider/Kingfisher-Journeys-Cayaking.jpg')">--}}
            <div class="item active" style="background:url('assets/images/kingfisher-min.jpeg')">
                <div class="container">
                    <div class="carousel-caption">
                        <h1 class="page-title"><?php echo $headertitle?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <content>
        <div class="about" style="border-bottom:5px solid #231f20;">
            <div class="container">
                <div class="row about-top">
                    <div class="col-sm-12 top-space">

                        <h1 style="font-weight: bold">TERMS AND CONDITIONS</h1><br>

                        <ul class="objective_list">
                            <li> <span>All fields must be duly completed in order for this submission to be eligible for consideration</span></li>
                            <li><span>You agree to the Rwanda Chamber of Tourism contacting you for the purpose of any necessary follow-up regarding your submission. Your personal data will be kept confidential and the Chamber of Tourism will not print your name without your permission.</span></li>
                            <li><span>Repeat nominations by the same individual, for the same establishment or professional in the same category, will be treated as a single nomination.</span></li>
                            <li><span>All information provided under Section 1 of this form may be reproduced for/within testimonials and feedback for publicity and marketing purposes for the TOURISM INDUSTRY GALA.</span></li>
                        </ul><br>
                        <div class="col-lg-10 col-lg-offset-5">
                            <img src="assets/images/footer.png" class="img-responsive" style="width: 100px;">
                        </div>
                        <div class="col-lg-12 text-center">
                            <div class="form-group checkbox-agree">
                                <label class="checkbox">
                                    <input type="checkbox" name="agree" required/>
                                    <div class="input input-option has-bender bender-topleft bender-border">
                                        <div class="input-option-marker"></div>
                                    </div>
                                    I agree to the above terms and conditions
                                </label>

                                <a href="{{url('NominateNow')}}" class="button button-secondary has-bender form-proceed"><strong>PROCEED</strong></a>

                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>

    </content>
    @include('layouts.footer')
@endsection