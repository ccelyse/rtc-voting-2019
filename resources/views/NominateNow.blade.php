@extends('layouts.master')

@section('title', 'Nominate Now')

@section('content')

    {{--@include('layouts.SecTopMenu')--}}
    <style>
        header:before {
            content: '';
            position: absolute;
            background-image: url(assets/images/slider/benjaminrobyn-jespersen-540757-unsplash.jpg) !important;
            background-size: cover;
            width: 100%;
            height: 100%;
        }
        header .page-title h1 {
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
        header .page-title p {
            color: #fff;
            font-size: 18px;
            line-height: 24px;
            width: 70%;
            margin: 0 auto;
        }
        .objective_list span {
            position: relative;
            bottom: 10px !important;
            color: #000;
        }
        .objective_list li {
            padding: 0px !important;
            margin-left: -20px;
        }
        a, * > a {
            color: #231f20;
            text-decoration: none;
            outline: none;
        }
        .btn:not(:disabled):not(.disabled) {
            cursor: pointer;
            color: #000;
            text-decoration: none;
            /* background: #000; */
        }
        .page-title{
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
        .alert-danger {
            color: #fff !important;
            background-color: #f9a44f;
            border-radius: 0px !important;
            border-color: #f9a44f !important;
        }
    </style>
    @include('layouts.topmenu')
    <div id="homeCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active" style="background:url('assets/images/kingfisher-min.jpeg')">
                <div class="container">
                    <div class="carousel-caption">
                        <h1 class="page-title"><?php echo $headertitle?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <content>
        <div class="public-nomination" style="border-bottom:5px solid #231f20;">
            <div class="container">

                    <div class="pubnom-top">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                            </div>
                        </div>
                    </div>
                    <div class="pubnom-option">
                        <div class="row">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
{{--                            <div class="col-sm-10 col-sm-offset-1">--}}
{{--                                <h3>Please select what you are nominating for</h3>--}}
{{--                                <div class="btn-group pubnom-option-toggle" data-toggle="buttons">--}}
{{--                                    <label class="btn btn-primary has-bender bender-topleft bender-border pubnom-option-individual ">--}}
{{--                                        <input type="radio" name="type" value="individual" id="individual" autocomplete="off" >PEOPLE AWARDS--}}
{{--                                    </label>--}}
{{--                                    <label class="btn btn-primary pubnom-option-establishment ">--}}
{{--                                        <input type="radio" name="type" value="establishment" id="establishment" autocomplete="off" >BUSINESS AWARDS--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                    <div class="pubnom-form">
                        <div class="row">
                            <div class="col-sm-10 col-sm-offset-1">
                                <div class="pubnom-form-title">
                                    <h4>1. NOMINEE INFORMATION</h4>
                                    <form class="form-horizontal form-simple" method="POST"
                                          action="{{ url('SendYourBusinessNominee') }}"
                                          enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>NOMINEE'S COMPANY NAME / if you are a tour guide provide your names*</label>
                                            <div class="input input-textfield has-bender bender-topleft bender-border">
                                                <input type="text" name="nomineecompanyname" placeholder="NOMINEE'S COMPANY NAME" value="" data-validation="required" data-validation-depends-on="type" data-validation-depends-on-value="individual" data-validation-error-msg="Please enter a valid name." required/>
                                                <span class="control-indicator"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>AWARD CATEGORY*</label>
                                            <p>Which category are you nominating the establishment for?</p>
                                            <div class="input input-textfield has-bender bender-topleft bender-border">
                                                <select style="width: 100%;" name="awardcategory" id="category_award" required>
                                                    <option value="">---- Select -----</option>
                                                    @foreach($listawardcat as $cat)
                                                        <option value="{{$cat->id}}">{{$cat->awardsubcategory}}</option>
                                                    @endforeach
                                                    <option></option>
                                                </select>
                                                <span class="control-indicator"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Sub Category*</label>
                                            <p>Which sub category are you nominating the establishment for? </p>
                                            <div class="input input-textfield has-bender bender-topleft bender-border">
                                                <select style="width: 100%;" name="people_sub_award" id="people_sub_award" required>

                                                </select>

                                                <span class="control-indicator"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Business Nominee's Picture</label>
                                            <div class="input input-textfield has-bender bender-topleft bender-border">
                                                <input type="file" name="nomineepicture" data-validation="required" accept="image/png, image/gif, image/jpeg" data-validation-depends-on="type" data-validation-depends-on-value="individual" data-validation-error-msg="Please enter a valid name." required/>
                                                <span class="control-indicator"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label id="experience-label">EXPERIENCE*(1000 characters)</label>
                                            <p id="experience-copy">Why do you think this establishment deserves the award?</p>
                                            <div class="input-textfield has-bender bender-topleft bender-border">
                                                <textarea id="experience-establishment" name="experience" placeholder="" rows="6" maxlength="1000" data-validation="required" data-validation-depends-on="type" data-validation-depends-on-value="establishment" data-validation-error-msg="Please enter a valid experience" required></textarea>
                                                <span class="control-indicator"></span>

                                            </div>
                                        </div>
                                        <div class="pubnom-form-title">
                                            <h4>2. PERSONAL INFORMATION</h4>
                                            <div class="pubnom-form-title">
                                                <h4>2. PERSONAL INFORMATION</h4>
                                                <p>Your personal information will be kept private & confidential,</p>
                                                <div class="form-group">
                                                    <label>NAME*</label>
                                                    <div class="input input-textfield has-bender bender-topleft bender-border">
                                                        <input name="sender_name" placeholder="FULL NAME" type="text" value="" data-validation="required" data-validation-error-msg="Please enter a valid name." required/>
                                                        <span class="control-indicator"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>EMAIL*</label>
                                                    <div class="input input-textfield has-bender bender-topleft bender-border">
                                                        <input name="sender_email" placeholder="name@email.com" type="email" value="" data-validation="email" data-validation-error-msg="Please enter a valid email." required/>
                                                        <span class="control-indicator"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Phone*</label>
                                                    <div class="input input-textfield has-bender bender-topleft bender-border">
                                                        <input name="sender_phonenumber"  type="number" placeholder="0782384772" value="" data-validation="number" data-validation-error-msg="Please enter a valid phone number." required/>
                                                        <span class="control-indicator"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="button button-secondary has-bender"><strong>SUBMIT</strong></button>
                                    </form>
                                </div>
{{--                                <div class="pubnom-form-content pubnom-form-nominee pubnom-form-individual ">--}}
{{--                                    <form class="form-horizontal form-simple" method="POST"--}}
{{--                                    action="{{ url('SendYourNominee') }}"--}}
{{--                                    enctype="multipart/form-data">--}}
{{--                                    {{ csrf_field() }}--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>NOMINEE'S NAME*</label>--}}
{{--                                        <div class="input input-textfield has-bender bender-topleft bender-border">--}}
{{--                                            <input type="text" name="nameofnominee" placeholder="NAME OF NOMINEE"  data-validation="required" data-validation-error-msg="Please enter a valid name." required/>--}}
{{--                                            <span class="control-indicator"></span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>ESTABLISHMENT* (if you are a freelance tour guide, please indicate it)</label>--}}
{{--                                        <p>Which establishment and outlet does the nominee work in?</p>--}}
{{--                                        <div class="input input-textfield has-bender bender-topleft bender-border">--}}
{{--                                            <input type="text" name="establishment" placeholder="NAME OF ESTABLISHMENT" value="" data-validation="required" data-validation-depends-on="type" data-validation-depends-on-value="individual" data-validation-error-msg="Please enter a valid establishment." required/>--}}
{{--                                            <span class="control-indicator"></span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>Category*</label>--}}
{{--                                        <p>Which category are you nominating her/him for </p>--}}
{{--                                        <div class="input input-textfield has-bender bender-topleft bender-border">--}}
{{--                                            <select style="width: 100%;" name="nomineecategory"  id="category_people" required>--}}
{{--                                                <option value="">---- Select -----</option>--}}
{{--                                                @foreach($listpeoplecat as $cat)--}}
{{--                                                <option value="{{$cat->id}}">{{$cat->awardsubcategory}}</option>--}}
{{--                                                @endforeach--}}
{{--                                            </select>--}}
{{--                                            <span class="control-indicator"></span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>Sub Category*</label>--}}
{{--                                        <p>Which sub category are you nominating her/him for </p>--}}
{{--                                        <div class="input input-textfield has-bender bender-topleft bender-border">--}}
{{--                                            <select style="width: 100%;" name="people_sub_cate" id="people_sub_cate" required>--}}

{{--                                            </select>--}}

{{--                                            <span class="control-indicator"></span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>NOMINEE'S Picture</label>--}}
{{--                                        <div class="input input-textfield has-bender bender-topleft bender-border">--}}
{{--                                            <input type="file" name="nomineepicture" data-validation="required" data-validation-depends-on="type" data-validation-depends-on-value="individual" data-validation-error-msg="Please enter a valid name." required/>--}}
{{--                                            <span class="control-indicator"></span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label>EXPERIENCE*(1000 characters) </label>--}}
{{--                                        <p>How the individual demonstrated excellence in his/her role, over and above his/her job description, in the past year. </p>--}}
{{--                                        <div class="input-textfield has-bender bender-topleft bender-border">--}}
{{--                                            <textarea id="experience-individual" name="experience"  rows="6" maxlength="1000" data-validation="required" data-validation-depends-on="type" data-validation-depends-on-value="individual" data-validation-error-msg="Please enter a valid experience." required></textarea>--}}
{{--                                            <span class="control-indicator"></span>--}}
{{--                                            <!--<div class="counter">-->--}}
{{--                                            <!--<ul>-->--}}
{{--                                            <!--<li class="has-bender bender-topleft bender-border"><span id="counter-individual">1000</span> chars</li>-->--}}
{{--                                            <!--</ul>-->--}}
{{--                                            <!--</div>-->--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="pubnom-form-title">--}}
{{--                                        <h4>2. PERSONAL INFORMATION</h4>--}}
{{--                                    </div>--}}
{{--                                    <div class="pubnom-form-content ">--}}
{{--                                        <p>Your personal information will be kept private & confidential,</p>--}}

{{--                                        <div class="form-group">--}}
{{--                                            <label>NAME*</label>--}}
{{--                                            <div class="input input-textfield has-bender bender-topleft bender-border">--}}
{{--                                                <input name="sender_name" placeholder="FULL NAME" type="text" value="" data-validation="required" data-validation-error-msg="Please enter a valid name." required/>--}}
{{--                                                <span class="control-indicator"></span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>EMAIL*</label>--}}
{{--                                            <div class="input input-textfield has-bender bender-topleft bender-border">--}}
{{--                                                <input name="sender_email" placeholder="name@email.com" type="email" value="" data-validation="email" data-validation-error-msg="Please enter a valid email." required/>--}}
{{--                                                <span class="control-indicator"></span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label>Phone*</label>--}}
{{--                                            <div class="input input-textfield has-bender bender-topleft bender-border">--}}
{{--                                                <input name="sender_phonenumber" placeholder="0782384772" type="number" value="" data-validation="number" data-validation-error-msg="Please enter a valid phone number." required/>--}}
{{--                                                <span class="control-indicator"></span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                    </div>--}}
{{--                                    <button type="submit" class="button button-secondary has-bender"><strong>SUBMIT</strong></button>--}}
{{--                                    </form>--}}
{{--                                </div>--}}


                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </content>
    {{--<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>--}}
    <script type="application/javascript">
        $(document).on('change', '#category_people', function() {
            var category_ids =$('#category_people').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "../api/ApiSubCategory",
                data: {
                    'id': category_ids,
                },
                success: function (response) {
                    JSON.stringify(response);
                    $.each(response, function (index, value) {
                        $("#people_sub_cate").empty();
                        $("#people_sub_cate").append("<option>--Select--</option>");
                        $(response).each(function(i){
                            $("#people_sub_cate").append("<option value="+response[i].id+">"+response[i].award_sub_category+"</option>");
                        });
                    });
                }, error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });
        });

        $(document).on('change', '#category_award', function() {
            var category_ids =$('#category_award').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "../api/ApiSubCategory",
                data: {
                    'id': category_ids,
                },
                success: function (response) {
                    JSON.stringify(response);
                    $.each(response, function (index, value) {
                        $("#people_sub_award").empty();
                        $("#people_sub_award").append("<option>--Select--</option>");
                        $(response).each(function(i){
                            $("#people_sub_award").append("<option value="+response[i].id+">"+response[i].award_sub_category+"</option>");
                        });
                    });
                }, error: function (xhr, status, error) {
                    console.log(xhr.responseText);
                }
            });
        });
    </script>
    @include('layouts.footer')
@endsection