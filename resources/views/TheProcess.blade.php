@extends('layouts.master')

@section('title', 'The Process')

@section('content')

    {{--@include('layouts.SecTopMenu')--}}
    <style>
        header:before {
            content: '';
            position: absolute;
            background-image: url(assets/images/slider/DkAHewlWsAA89Z7.jpg) !important;
            background-size: cover;
            width: 100%;
            height: 100%;
        }
        header .page-title h1 {
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
        header .page-title p {
            color: #fff;
            font-size: 18px;
            line-height: 24px;
            width: 70%;
            margin: 0 auto;
        }
        a, * > a {
            color: #231f20;
            text-decoration: none;
            outline: none;
        }
        .page-title{
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
    </style>
    @include('layouts.topmenu')
    <div id="homeCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active" style="background:url('assets/images/slider/DkAHewlWsAA89Z7.jpg')">
                <div class="container">
                    <div class="carousel-caption">
                        <h1 class="page-title"><?php echo $headertitle?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <content>
        <div class="the-process" style="border-bottom: 5px solid #231f20;">
            <div class="container">
                <div class="row first">
                    <div class="col-md-2 col-sm-3">
                        <div class="row">
                            <div class="date-wrapper">
                                <div class="process-date">
                                    8<span>Nov'22</span>
                                </div>
                                <div class="date-divider"></div>
                                <div class="process-date">
                                    20<span>Nov'22</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 has-timeline">
                        <div class="timeline"></div>
                        <h2 class="process-title">Nomination and voting for 2022 Awards</h2>
                                <p>Note: Nominations made after 3rd December 2022 will be considered for the 2023 edition.</p>
                                <p class="process-subtitle">1. <a href="{{url('Nominate')}}" class="button button-secondary"><strong>nominate now</strong></a></p>
                                <p class="process-subtitle">2. <a href="{{url('VoteNow')}}" class="button button-secondary"><strong>Vote now</strong></a></p>


                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2 col-sm-3">
                        <div class="row">
                            <div class="date-wrapper">
                                <div class="process-date">
                                    20 Dec'22
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 has-timeline">
                        <div class="timeline"></div>
                        <h2 class="process-title">Judging Process</h2>
                            <ul class="objective_list">
                                <li> <span>Online Votes account for 40%. </span></li>
                                <li><span>60% of the total votes are determined by judging panel.</span></li>
                            </ul>
                    </div>
                </div>

                <div class="row last">
                    <div class="col-md-2 col-sm-3">
                        <div class="row">
                            <div class="date-wrapper">
                                <div class="process-date">
                                    3 Dec'22
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9 has-timeline">
                        <div class="timeline"></div>
                        <h2 class="process-title">Winners will be celebrated at the tourism awards Gala</h2>
                        {{--<p>Winners will be celebrated at the industry Gala.</p>--}}
                        <p>For any clarification , please contact:info@rwandatourismchamber.org</p>
                    </div>
                </div>
            </div>
        </div>

    </content>
    @include('layouts.footer')
@endsection