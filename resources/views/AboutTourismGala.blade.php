@extends('layouts.master')

@section('title', 'About RWANDA TOURISM INDUSTRY NIGHT')

@section('content')

    {{--@include('layouts.SecTopMenu')--}}
    <style>
        header:before {
            content: '';
            position: absolute;
            background-image: url(assets/images/slider/DkAHewlWsAA89Z7.jpg) !important;
            background-size: cover;
            width: 100%;
            height: 100%;
        }
        header .page-title h1 {
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
        header .page-title p {
            color: #fff;
            font-size: 18px;
            line-height: 24px;
            width: 70%;
            margin: 0 auto;
        }
        a, * > a {
            color: #231f20;
            text-decoration: none;
            outline: none;
        }
        .page-title{
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
    </style>
    @include('layouts.topmenu')
    <div id="homeCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active" style="background:url('assets/images/slider/DixdgguX4AAOsR3-min.jpg')">
                <div class="container">
                    <div class="carousel-caption">
                        <h1 class="page-title"><?php echo $headertitle?></h1>
                        <p><?php echo $para?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <content>
        <div class="about" style="border-bottom: 5px solid #231f20;">
            <div class="container">
                <div class="row about-top">
                    <!--<div class="col-sm-5 top-space">-->
                    <!--<img src="assets/images/slider/DixdgguX4AAOsR3.jpg">-->
                    <!--</div>-->
                    <div class="col-md-12">
                        <p>
                            From <strong>November 26th to December 3rd, 2022,</strong> the Rwanda Chamber of Tourism, together with its stakeholders from the continent and the globe, will celebrate tourism through a week-long of events. The Rwanda Tourism week gala dinner will take place on <strong>December 3rd, 2022</strong>, and will be a great occasion to celebrate Rwanda and Africa’s tourism and an opportunity for networking among tourism and conservation stakeholders.
                        </p>
                    </div>
                    {{--<div class="col-sm-6 top-space">--}}
                        {{--<img src="assets/images/slider/invitation.jpg" class="img-responsive">--}}
                        {{--<p></p>--}}
                    {{--</div>--}}

                    <div class="col-sm-12 top-space">
                        <p></p>
                        <h1 style="font-weight: bold;margin-bottom: 10px">OBJECTIVES OF RTW 2022:  </h1>
                        <ul class="objective_list">
                            <li> <span>To promote domestic, intra-regional, and continental tourism businesses. </span></li>
                            <li><span>To promote inclusive Tourism business, focusing on youth and women’s full participation. </span></li>
                            <li><span>To showcase innovation and technology to stimulate tourism trade benefits across Africa. </span></li>
                            <li><span>To establish and enhance public-private sector collaboration among key travel trade stakeholders.</span></li>
                            <li><span>To promote public-private sector dialogue focusing on conservation best practices for Sustainable Tourism businesses.</span></li>
                        </ul>
                    </div>

{{--                    <div class="col-sm-6 top-space">--}}
{{--                        <h1 style="font-weight: bold">OUTCOME </h1>--}}
{{--                        <ul class="objective_list">--}}
{{--                            <li> <span>Improved performance and motivation of sector players towards creating industry champions</span></li>--}}
{{--                            <li><span>This year’s event to serve as a commitment to organizing the Industry Awards on an ongoing annual basis.</span></li>--}}
{{--                            <li><span>Improved public relations: Sector achievers, successes and plans effectively communicated within the sector and beyond.</span></li>--}}
{{--                            <li><span>Increased efforts to raise the Rwanda tourism sector's competitiveness by motivating organizations (companies and entrepreneurs), to create compelling experiences or adopt best practices that are eco-friendly, sustainable and impactful.</span></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
                    <div class="col-sm-12 top-space">
                        <h1 style="font-weight: bold">PURPOSE AND RATIONALE</h1>
                        <p>The purpose of the gala dinner and Tourism Excellence Awards event is to: <br><br>
                            a) Appreciate and recognize sector players for the efforts implemented on the road to recovery from the impact of COVID -19<br> <br>
                            b) Establish a landmark event in the country that will bring together all actors in the industry to celebrate the achievements in the sector;  <br><br>
                            c) Create a platform to attract potential stakeholders into the investment opportunities in the sector .<br><br>
                            d) Appreciate and recognize the importance of all the operators working collaboratively across the sector.
                        </p>

                    </div>

                    <div class="col-sm-12 top-space">
                        <h1 style="font-weight: bold">TARGET AUDIENCE AND VENUE</h1>
                        <p>
                            Over 400 guests are expected to attend the gala dinner on December 3rd, 2022. These include high-level government and private sector officials,
                            speakers at the Africa Tourism Business Forum, sponsors, regional and international tourism ministries, foreign embassies, fam trip participants,
                            and other distinguished delegates of the Rwanda Tourism Week 2022. The Gala dinner will take place at Kigali Convention Center from 7 pm.
                        </p>

                    </div>

                    <div class="col-sm-12 top-space">
                        <h1 style="font-weight: bold">PARTICIPATION IN THE TOURISM EXCELLENCE AWARDS</h1>
                        <p>All businesses that are members of the associations under the Rwanda Chamber of Tourism are eligible to participate in the tourism excellence awards. The associations are: </p>
                        <ul class="objective_list">
                            <li> <span>Rwanda Hospitality Association</span></li>
                            <li><span>Rwanda Tours and Travel Association</span></li>
                            <li><span>Rwanda Safari Guides Association</span></li>
                            <li><span>Rwanda Hospitality and Tourism Educators Association</span></li>
                            <li><span>Rwanda Travel Agencies Association</span></li>
                            <li><span>Rwanda Community-Based Tourism Initiatives</span></li>
                            <li><span>Rwanda Professional Conference Organizers</span></li>
                        </ul>
                    </div>

                    <div class="col-sm-12 top-space">
                        <h1 style="font-weight: bold">REQUIREMENTS/GUIDELINES FOR NOMINATION</h1>
                        <p>All businesses that are members of the associations under the Rwanda Chamber of Tourism are eligible to participate in the tourism excellence awards. The associations are: </p>
                        <ul class="objective_list">
                            <li> <span>Must be an active and fully paid member of the Chamber of Tourism </span></li>
                            <li><span>Must have a Tourism Operating License</span></li>
                            <li><span>Must have experience in Tourism for at least 2 years </span></li>
                            <li><span>Must apply or be nominated online to be part of the competition </span></li>
                        </ul>
                    </div>

                </div>
                <div class="row about-top">
                    <div class="col-sm-11 col-sm-offset-1">

                    </div>
                </div>

                <div class="row about-bottom">
                    <div class="col-sm-4 fullscreen-xs about-post">

                        <div class="post-content">
                            <a href="#"><h3>Award Categories</h3></a>

                            <p>Promoting growth and diversity through tourism</p>
                            <a href="{{url('MoreAwardCategories')}}"><i class="fa fa-angle-right" aria-hidden="true"></i>FIND OUT MORE</a>
                        </div>
                    </div>
                    <div class="col-sm-4 fullscreen-xs about-post">

                        <div class="post-content">
                            <a href="#"><h3>Our Judges 2022</h3></a>
                            <p>Our panel of esteemed judges are selected for their knowledge, expertise and integrity.</p>
                        </div>
                    </div>
                    <div class="col-sm-4 fullscreen-xs about-post">

                        <div class="post-content">
                            <a href="#"><h3>The Process</h3></a>
                            <p>Nominations and voting are open from November 8th to November 20th, 2022. Award winners will be announced on 3rd December 2022.</p>
                            <a href="{{url('TheProcess')}}"><i class="fa fa-angle-right" aria-hidden="true"></i>FIND OUT MORE</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </content>
    @include('layouts.footer')
@endsection