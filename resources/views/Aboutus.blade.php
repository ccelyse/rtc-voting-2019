@extends('layouts.master')

@section('title', 'About Chamber of Tourism')

@section('content')

    {{--@include('layouts.SecTopMenu')--}}
    <style>
        header:before {
            content: '';
            position: absolute;
            background-image: url(assets/images/conve-min.jpg) !important;
            background-size: cover;
            width: 100%;
            height: 100%;
        }
        header .page-title h1 {
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
        header .page-title p {
            color: #fff;
            font-size: 18px;
            line-height: 24px;
            width: 70%;
            margin: 0 auto;
        }
        .page-title{
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
    </style>
    @include('layouts.topmenu')
    <div id="homeCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active" style="background:url('assets/images/conve-min.jpg')">
                <div class="container">
                    <div class="carousel-caption">
                        <h1 class="page-title"><?php echo $headertitle?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <content>
        <div class="about" style="border-bottom:5px solid #231f20;">
            <div class="container">
                <div class="row about-top">
                    <!--<div class="col-sm-5 top-space">-->
                    <!--<img src="assets/images/slider/DixdgguX4AAOsR3.jpg">-->
                    <!--</div>-->

                    <!--<div class="col-sm-6 top-space">-->
                    <!--<img src="assets/images/slider/Kigali012.JPG" class="img-responsive">-->
                    <!--</div>-->

                    <div class="col-sm-6 top-space">
                        <h1 style="font-weight: bold">OUR ORIGIN </h1>
                        <p>The Rwanda Chamber of Tourism is one of the 10 professional chambers that currently exist under the umbrella of the Private Sector Federation (PSF), which is dedicated to promoting and representing the interests of the Rwandan business community. </p>
                        <p>It was established in December 1999, replacing the former Rwanda Chamber of Commerce and Industry. Following the establishment of the PSF, the Rwanda Chamber for Tourism was established in 2006, with a mandate of enhancing business opportunities through effective lobbying, advocacy and capacity building for the tourism and hospitality industry in Rwanda.</p>

                        <h1 style="font-weight: bold">Vision </h1>
                        <p>To ensure profitable and sustainable tourism businesses for a prosperous Rwanda.</p>

                        <h1 style="font-weight: bold">Mission </h1>
                        <p>To effectively advocate and reinforce an enabling business environment for its members.</p>

                    </div>
                    <div class="col-sm-6 top-space">
                        <h1 style="font-weight: bold">Core Objectives </h1><br>
                        <ul class="objective_list">
                            <li><span>Lobbying and Advocacy: Building a strong a pro-active advocacy platform</span></li>
                            <li><span>Membership: Developing a member-focused service delivery among its members</span></li>
                            <li><span>Communication: Efficient communication, Networking and Information Exchange</span></li>
                            <li><span>Capacity building: Building the capacity of its members  </span></li>
                            <li><span>Resources: Investing in the right staff and processes to deliver better services</span></li>
                            <li><span>Creation of strong business linkages : through networking platforms within the industry and beyond</span></li>
                            <li><span>Promotion and Marketing : enabling industry actors to take advantage of the sector's extensive multi-channel marketing programme.</span></li>
                        </ul>

                        <h1 style="font-weight: bold">Core Values </h1><br>
                        <ul class="objective_list">
                            <li> <span>Accountability and transparency</span></li>
                            <li><span>Environmental protection and conservation</span></li>
                            <li><span>Socio-economic impact on communities through tourism</span></li>
                            <li><span>Capacity building: Building the capacity of its members  </span></li>
                        </ul>
                    </div>

                </div>

            </div>
        </div>
    </content>

@include('layouts.footer')
@endsection