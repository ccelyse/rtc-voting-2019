<header class="home-header">
    <div class="container main-header">
        <div class="row">
            <div class="logo hidden-xs col-sm-2">
                <a href="{{url('/')}}"></a>
            </div>
            <div class="navbar-header hidden-xs col-xs-10">
                <div class="nav">
                    <ul class="main-nav">
                        <li class="
                              current
                              ">
                            <a href="{{url('/')}}" class="" target="_self">
                                HOME
                            </a>
                        </li>
                        <li class="
                              current
                              ">
                            <a href="{{url('AboutUs')}}" class="" target="_self">
                                About Us
                            </a>
                        </li>
                        <li class="
                              current
                              ">
                            <a href="{{url('MemberDirectory')}}" class="" target="_self">
                                Members
                            </a>
                        </li>
                        <li class="
                              current
                              ">
                            <a href="{{url('AboutTourismGala')}}" class="" target="_self">
                                Tourism awards Gala
                            </a>
                        </li>

                        <li class="
                              ">
                            <a href="{{url('Nominate')}}" class="" target="_self">
                                Nominate Now
                            </a>
                        </li>
                        <li class="
                              ">
                            <a href="{{url('ContactUs')}}" class="" target="_self">
                                Contact Us
                            </a>
                        </li>
                        <li class="
                              ">
                            <a href="{{url('login')}}" class="" target="_self">
                                Log in
                            </a>
                        </li>
                    </ul>
                </div>
            </div>


        </div>
    </div>
    <div class="header-fixed">
        <div class="container">
            <div class="row">
                <div class="navbar-header col-xs-10">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div id="navbar" class="nav collapse navbar-collapse">
                        <ul class="main-nav">
                            <li class="
                              current
                              ">
                                <a href="{{url('/')}}" class="" target="_self">
                                    HOME
                                </a>
                            </li>
                            <li class="
                              current
                              ">
                                <a href="{{url('AboutUs')}}" class="" target="_self">
                                    About Us
                                </a>
                            </li>
                            <li class="
                              current
                              ">
                                <a href="{{url('MemberDirectory')}}" class="" target="_self">
                                    Members
                                </a>
                            </li>
                            <li class="
                              current
                              ">
                                <a href="{{url('AboutTourismGala')}}" class="" target="_self">
                                    Tourism awards Gala
                                </a>
                            </li>

                            <li class="
                              ">
                                <a href="{{url('Nominate')}}" class="" target="_self">
                                    Nominate Now
                                </a>
                            </li>
                            <li class="
                              ">
                                <a href="{{url('ContactUs')}}" class="" target="_self">
                                    Contact Us
                                </a>
                            </li>
                            <li class="
                              ">
                                <a href="{{url('login')}}" class="" target="_self">
                                    Log in
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="header-frame hidden-xs">
        <img src="app/sta/assets/addons/sta/egplus/statheme-theme/resources/images/header-frame.svg">
    </div>
</header>