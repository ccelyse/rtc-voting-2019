<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Rwanda Tourism Awards Gala”is an annual event that is organized with the aim to bring together all the tourism industry stakeholders in the private and public sectors to celebrate the industry’s achievements and recognize the industry's contribution to the socio-economic growth of the country.">
    <meta name="author" content="Elysee Confiance">

    {{--<meta property="og:title" content="Rwanda Tourism Gala Awards" />--}}
    {{--<meta property="og:description" content="Rwanda Tourism Industry Gala”is an annual event that is organized with the aim to bring together all the tourism industry stakeholders in the private and public sectors to celebrate the industry’s achievements and recognize the industry's contribution to the socio-economic growth of the country." />--}}
    {{--<meta property="og:image" content="assets/images/logo.png" />--}}
    <title>@yield('title')</title>
    <link rel="stylesheet" href="assets/font/all.css">
    <script src="assets/font/all.min.js" type="text/javascript"></script>
    <link media="all" type="text/css" rel="stylesheet" href="app/sta/assets/public/theme.css">
    <script src="app/sta/assets/public/jquery.2.1.3.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta property="fb:app_id" content="293883694598098">
    <?php
    $url = Request::fullUrl();
    ?>
    <meta property="og:url" content="{{$url}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="RWANDA TOURISM AWARDS GALA">
    <meta property="og:image" content="{!! asset('assets/images/slider/DkAHewlWsAA89Z7.jpg') !!}">
    <meta property="og:description" content="“Rwanda Tourism Awards Gala”is an annual event that is organized with the aim to bring together all the tourism industry stakeholders in the private and public sectors to celebrate the industry’s achievements and recognize the industry's contribution to the socio-economic growth of the country.">
    <meta property="og:site_name" content="RWANDA TOURISM AWARDS GALA">
    <meta property="og:locale" content="en_US">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-120820678-5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-120820678-5');
    </script>


    {{--<meta name="twitter:card" content="Rwanda Tourism Industry Gala”is an annual event that is organized with the aim to bring together all the tourism industry stakeholders in the private and public sectors to celebrate the industry’s achievements and recognize the industry's contribution to the socio-economic growth of the country.">--}}
    {{--<meta name="twitter:site" content="website">--}}
    {{--<meta name="twitter:creator" content="@ElyseeConfiance">--}}
    {{--<meta name="twitter:url" content="{{url('/')}}">--}}
    {{--<meta name="twitter:title" content="RWANDA TOURISM INDUSTRY GALA">--}}
    {{--<meta name="twitter:description" content="“Rwanda Tourism Industry Gala”is an annual event that is organized with the aim to bring together all the tourism industry stakeholders in the private and public sectors to celebrate the industry’s achievements and recognize the industry's contribution to the socio-economic growth of the country.">--}}
    {{--<meta name="twitter:image" content="{!! asset('assets/images/slider/DkAHewlWsAA89Z7.jpg') !!}">--}}
</head>

<body>
@yield('content')