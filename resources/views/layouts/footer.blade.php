<style>
    /*Message to client */

    #messageToClient {
        /*background-color: rgba(0, 0, 0, 0.68);*/
        min-height: 26px;
        font-size: 14px;
        color: #ccc;
        line-height: 26px;
        padding: 8px 0 8px 30px;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif !important;
        position: fixed;
        bottom: 10%;
        left: 0;
        right: 0;
        display: none;
        z-index: 9999;
    }

    #messageToClient a {
        color: #4B8EE7;
        text-decoration: none;
    }

    #messageToClient li {
        font-size: 12px;
        border-left: 4px solid #44a548;
        font-weight: 900;
    }

    #closeMessageToClient {
        float: right;
        display: inline-block;
        cursor: pointer;
        height: 20px;
        width: 20px;
        margin: -15px 0 0 0;
        font-weight: bold;
    }

    #closeMessageToClient:hover {
        color: #FFF;
    }

    #messageToClient a.messageOK {

        color: #fff;
        border-radius: 5px;
        padding: 0px 30px;
        cursor: pointer;
        font-size: 30px;
        position: absolute;
        top: 0;
        right: 0;
    }
    #messageToClient a.messagevisit {

        color: #fff;
        /* border-radius: 5px; */
        padding: 10px 10px;
        cursor: pointer;
        font-size: 16px;
        position: absolute;
        bottom: 0;
        right: 0;
        /* left: 0; */
        margin-right: 15px;
    }

    #messageToClient a.messageOK:hover {
        color: #231f20;
    }
    #imagesbanner img{
        float: right;
    }

    /*message to client end*/
</style>
{{--<div id="messageToClient">--}}
    {{--<div id="closeMessageToClient">x</div>--}}
    {{--<div class="container">--}}
        {{--<div class="col-md-12" id="imagesbanner">--}}
            {{--<p>--}}
                {{--<img src="assets/images/Rwanda-Web-Baner_300x400.jpg">--}}
            {{--</p>--}}
                {{--<a class="messageOK"><i class="fas fa-times-circle"></i></a>--}}
            {{--<a href="http://www.landmarine.org/media-pack/RWANDA-Tourism-Guide-2019-20.pdf" target="_blank" class="messagevisit button button-secondary has-bender form-proceed">Find out more</a>--}}
        {{--</div>--}}

    {{--</div>--}}
{{--</div>--}}
<footer>
    <div class="lower">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 footer-copyright">
                    <div class="footer-content">
                        <p>Copyright &copy; 2022 <a href="#" target="_blank">Rwanda Chamber Tourism.</a></p>
                        <p>Powered By <a href="http://www.sd.rw" target="_blank"><strong>Skyline Digital</strong></a></p>
                    </div>
                </div>
                <div class="col-lg-6 footer-copyright">
                    <img src="assets/images/footer.png" class="img-responsive" style="width: 150px;position: absolute;/* left: 0; */right: 0;top: 0;bottom: 0;">
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Modal -->
<div class="modal fade" id="globalModal" tabindex="-1" role="dialog" aria-labelledby="globalModalLabel">
    <div class="modal-dialog" role="document">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="modal-content modal-bender">
            <div class="modal-body"></div>
        </div>
    </div>
</div>

<script src="app/sta/assets/public/theme.js"></script>
<script>

    $(document).ready(function () {
        $(".arrow-right").bind("click", function (event) {
            event.preventDefault();
            $(".vid-list-container").stop().animate({
                scrollLeft: "+=336"
            }, 750);
        });
        $(".arrow-left").bind("click", function (event) {
            event.preventDefault();
            $(".vid-list-container").stop().animate({
                scrollLeft: "-=336"
            }, 750);
        });
    });
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
<script>
    $(document).ready(function(){
        setTimeout(function () {
            $("#messageToClient").fadeIn(200);
        }, 4000);
        $("#closeMessageToClient, .messageOK").click(function() {
            $("#messageToClient").fadeOut(200);
        });
    });
</script>

<script src='https://www.google.com/recaptcha/api.js'></script>

<script type="text/javascript">
    _satellite.pageBottom();
</script>

</body>

</html>