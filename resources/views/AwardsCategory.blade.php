<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="description" content="Rwanda Tourism Industry Gala”is an annual event that is organized with the aim to bring together all the tourism industry stakeholders in the private and public sectors to celebrate the industry’s achievements and recognize the industry's contribution to the socio-economic growth of the country.">
    <meta name="author" content="Elysee Confiance">

    {{--<meta property="og:title" content="Rwanda Tourism Gala Awards" />--}}
    {{--<meta property="og:description" content="Rwanda Tourism Industry Gala”is an annual event that is organized with the aim to bring together all the tourism industry stakeholders in the private and public sectors to celebrate the industry’s achievements and recognize the industry's contribution to the socio-economic growth of the country." />--}}
    {{--<meta property="og:image" content="assets/images/logo.png" />--}}
    <title>@yield('title')</title>
    <link rel="stylesheet" href="assets/font/all.css">
    <script src="assets/font/all.min.js" type="text/javascript"></script>
    <link media="all" type="text/css" rel="stylesheet" href="app/sta/assets/public/theme.css">
    <script src="app/sta/assets/public/jquery.2.1.3.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--<meta property="fb:app_id" content="293883694598098">--}}
    <?php
    $url = Request::fullUrl();
    ?>
    <meta property="og:url" content="{{$url}}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="RWANDA TOURISM INDUSTRY GALA">
    <meta property="og:image" content="{!! asset('assets/images/slider/DkAHewlWsAA89Z7.jpg') !!}">
    <meta property="og:description" content="“Rwanda Tourism Industry Gala”is an annual event that is organized with the aim to bring together all the tourism industry stakeholders in the private and public sectors to celebrate the industry’s achievements and recognize the industry's contribution to the socio-economic growth of the country.">
    <meta property="og:site_name" content="RWANDA TOURISM INDUSTRY GALA">
    <meta property="og:locale" content="en_US">
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId            : '293883694598098',
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v2.10'
            });
            FB.AppEvents.logPageView();
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    {{--<meta name="twitter:card" content="Rwanda Tourism Industry Gala”is an annual event that is organized with the aim to bring together all the tourism industry stakeholders in the private and public sectors to celebrate the industry’s achievements and recognize the industry's contribution to the socio-economic growth of the country.">--}}
    {{--<meta name="twitter:site" content="website">--}}
    {{--<meta name="twitter:creator" content="@ElyseeConfiance">--}}
    {{--<meta name="twitter:url" content="{{url('/')}}">--}}
    {{--<meta name="twitter:title" content="RWANDA TOURISM INDUSTRY GALA">--}}
    {{--<meta name="twitter:description" content="“Rwanda Tourism Industry Gala”is an annual event that is organized with the aim to bring together all the tourism industry stakeholders in the private and public sectors to celebrate the industry’s achievements and recognize the industry's contribution to the socio-economic growth of the country.">--}}
    {{--<meta name="twitter:image" content="{!! asset('assets/images/slider/DkAHewlWsAA89Z7.jpg') !!}">--}}
</head>

<body>
@include('layouts.SecTopMenu')

<style>
    header:before {
        content: '';
        position: absolute;
        background-image: url(assets/images/slider/20062978989_2ce115f88d_o.jpg) !important;
        background-size: cover;
        width: 100%;
        height: 100%;
    }
    header .page-title h1 {
        font-family: 'OpenSans';
        font-size: 36px;
        letter-spacing: 0.08em;
        color: #fff;
        text-transform: uppercase;
        font-weight: bold;
    }
    header .page-title p {
        color: #fff;
        font-size: 18px;
        line-height: 24px;
        width: 70%;
        margin: 0 auto;
    }
    .objective_list span {
        position: relative;
        bottom: 10px !important;
        color: #000;
    }
    .objective_list li {
        padding: 0px !important;
        margin-left: -20px;
    }
    a, * > a {
        color: #231f20;
        text-decoration: none;
        outline: none;
    }
    .winners .winners-list .winners-finalists .winners-finalists-list .winners-finalists-listpost, .past-winners .winners-list .winners-finalists .winners-finalists-list .winners-finalists-listpost{
        padding: 0px;
    }
    p {
        text-transform: none;
        letter-spacing: 0.05em;
        color: #000;
    }
    .modal .modal-dialog .modal-content {
        border-radius: 0;
        border: 2px solid #221f20;
        background: white;
        box-shadow: none;
    }
    .modal .modal-dialog .close {
        position: absolute;
        right: 0;
        top: 0;
        margin-top: -25px;
        text-shadow: none;
        color: #221f20;
        opacity: 1;
    }
</style>
<content>

    <div class="content winners" style="    border-bottom: 5px solid #231f20;">
        <div class="container">
            <div class="winners-list">
                <div class="row">
                    @if(0 == count($nominees))
                        <h1 style='padding: 100px;text-align: center !important;'>We don't yet have Nominee in this category.</h1>
                    @else()

                        @if($picturepath == 'peopleaward')
                            @foreach($nominees as $nomineeinfo)
                                <div class="col-sm-10 col-sm-offset-1" style="padding-top: 15px;">
                                    <div class="fullscreen-xs col-sm-6 col-lg-4">
                                        <div class="img-has-frame">

                                            <img src="<?php echo "$picturepath";?>/{{$nomineeinfo->nomineepicture}}" alt="{{$nomineeinfo->nameofnominee}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-8">
                                        <div class="winners-description">
                                            <h4>{{$nomineeinfo->nameofnominee}}</h4>
                                            <p class="winners-location">{{$nomineeinfo->establishment}}</p>
                                            <p style="margin-top: 10px">{{$nomineeinfo->experience}}</p>
                                            <div class="winners-post-action">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="winners-finalists">
                                        <div class="row">
                                            <div class="winners-finalists-list">

                                                <div class="col-sm-4" style="padding-top: 10px">
                                                    <div class="winners-finalists-listpost">
                                                        <a href=" {{ route('VoteYourNominee',['id'=> $nomineeinfo->id,'cat'=>'People Award'])}}" class="button button-secondary has-bender form-proceed">
                                                            Vote
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4" style="padding-top: 10px">
                                                    <div class="winners-finalists-listpost">
                                                        <p style="font-weight: bold">
                                                            <?php
                                                            $nominee_id = $nomineeinfo->id;
                                                            $nominee_category = $id;
//                                                            $votes = \App\VotingNominee::select(DB::raw('count(id) as votes'))->where('nominee_id',$nominee_id)->where('nominee_category',$nominee_category)->where('votestatus','1')
//                                                                ->value('votes');
                                                            $votes = $nomineeinfo->VotesNumber;

                                                            $votesofemail = \App\VoteWithEmail::select(DB::raw('count(id) as votes'))->where('nominee_id',$nominee_id)->where('nominee_category',$nominee_category)->where('votestatus','1')
                                                                ->value('votes');
                                                            $votestotal = \App\VotingNominee::select(DB::raw('count(id) as votes'))->where('nominee_category',$nominee_category)->where('votestatus','1')
                                                                ->value('votes');
                                                            $emailwithsmsvotes = $votes + $votesofemail;

                                                            if ($votes ==0 and $votestotal == 0){
                                                                echo "Number of Votes: 0";
                                                            }else{
                                                                $votesinpercentage = ($votes / $votestotal)*100;
                                                                echo "Number of Votes: $votes";
                                                            }
                                                            ?>
                                                        </p>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-10 col-sm-offset-1">
                                    <div class="separatornominee"></div>
                                </div>
                            @endforeach
                        @else()
                            @foreach($nominees as $nomineeinfo)
                                <div class="col-sm-10 col-sm-offset-1" style="padding-top: 15px;">
                                    <div class="fullscreen-xs col-sm-6 col-lg-4">
                                        <div class="img-has-frame">

                                            <img src="<?php echo "$picturepath";?>/{{$nomineeinfo->nomineepicture}}" alt="{{$nomineeinfo->nomineecompanyname}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-8">
                                        <div class="winners-description">
                                            <h4>{{$nomineeinfo->nomineecompanyname}}</h4>
                                            {{--<p class="winners-location">{{$nomineeinfo->awardcategory}}</p>--}}
                                            <p>{{$nomineeinfo->experience}}</p>
                                            <div class="winners-post-action">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="winners-finalists">
                                        <div class="winners-finalists-list">
                                            <div class="col-sm-4">
                                                <div class="winners-finalists-listpost">
                                                    <a href="#" class="button button-secondary has-bender form-proceed">
                                                        Voting has been closed
                                                    </a>
{{--                                                    <a href=" {{ route('VoteYourNominee',['id'=> $nomineeinfo->id,'cat'=>'businessaward'])}}" class="button button-secondary has-bender form-proceed">--}}
{{--                                                        Vote--}}
{{--                                                    </a>--}}

                                                </div>
                                            </div>
                                            <div class="col-sm-4" style="padding-top: 10px">
                                                <div class="winners-finalists-listpost">
                                                    <p style="font-weight: bold">
                                                        <?php
                                                        $nominee_id = $nomineeinfo->id;
                                                        $nominee_category = $id;
//                                                        $votes = \App\VotingNominee::select(DB::raw('count(id) as votes'))->where('nominee_id',$nominee_id)->where('nominee_category',$nominee_category)->where('votestatus','1')
//                                                            ->value('votes');
                                                        $votes = $nomineeinfo->VotesNumber;
                                                        $votesemail = $nomineeinfo->VotesNumberEmail;

//                                                        $votesofemail = \App\VoteWithEmail::select(DB::raw('count(id) as votes'))->where('nominee_id',$nominee_id)->where('nominee_category',$nominee_category)->where('votestatus','1')
//                                                            ->value('votes');
                                                        $votestotal = \App\VotingNominee::select(DB::raw('count(id) as votes'))->where('nominee_category',$nominee_category)->where('votestatus','1')
                                                            ->value('votes');

                                                        $emailwithsmsvotes = $votes + $votesemail;

                                                        if ($votes ==0 and $votestotal == 0){
                                                            echo "Number of Votes: 0";
                                                        }else{
                                                            $votesinpercentage = ($votes / $votestotal)*100;
                                                            echo "Number of Votes: $emailwithsmsvotes";
                                                        }
                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-10 col-sm-offset-1">
                                    <div class="separatornominee"></div>
                                </div>
                            @endforeach

                        @endif

                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>

</content>

@include('layouts.footer')
