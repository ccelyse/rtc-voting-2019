@extends('layouts.master')

@section('title', 'Award categories')

@section('content')

    {{--@include('layouts.SecTopMenu')--}}
    <style>
        .card-header {
            padding: .75rem 1.25rem;
            margin-bottom: 0;
            background-color: #f9a44f !important;
            border-bottom: 1px solid #f9a44f !important;
            border-radius: 0px !important;
        }
        .card {
            position: relative;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            -ms-flex-direction: column;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid rgb(249, 164, 79) !important;
            border-radius: 0px !important;
            margin-top: 10px !important;
        }
        header:before {
            content: '';
            position: absolute;
            background-image: url(assets/images/slider/DnjFf4rXoAAODRG.jpg) !important;
            background-size: cover;
            width: 100%;
            height: 100%;
        }
        header .page-title p {
            color: #fff;
            font-size: 18px;
            line-height: 24px;
            width: 70%;
            margin: 0 auto;
        }
        .objective_list span{
            position: relative;
            bottom: 0px !important;
        }
        .page-title{
            padding-top: 330px !important;
        }
        .page-title{
            font-family: 'OpenSans';
            font-size: 36px;
            letter-spacing: 0.08em;
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }
        header .navbar-header .nav a {
            color: #fff;
            font-size: 12px !important;
            text-transform: uppercase;
            letter-spacing: 0.08em;
            font-weight: bold;
        }
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    @include('layouts.topmenu')
    <div id="homeCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active" style="background:url('assets/images/slider/DnjFf4rXoAAODRG.jpg')">
                <div class="container">
                    <div class="carousel-caption">
                        <h1 class="page-title"><?php echo $headertitle?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <content>

        <div class="content award-cat" style="    border-bottom: 5px solid #231f20">
            <div class="container">
                <div class="row about-top" style="margin-top: 10px">
                    <!--<div class="col-sm-5 top-space">-->
                    <!--<img src="assets/images/slider/DixdgguX4AAOsR3.jpg">-->
                    <!--</div>-->
                    {{--<div class="col-sm-6 top-space">--}}
                        {{--<img src="assets/images/slider/DnjFf4rXoAAODRG.jpg" class="img-responsive">--}}
                    {{--</div>--}}
                    <div class="col-sm-6">
                        <p style="color: #000;">The Rwanda Hospitality Association (RHA), will award the following: </p>
                        <h5>1.	Hotels and Lodges Category</h5>
                        <ul class="sf-list ">
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading Town Hotel (Mid- Upper range)</span></li>
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading vacation Hotel/ Resort</span></li>
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading Boutique Hotel</span></li>
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading Boutique Hotel</span></li>
                        </ul>
                        <h5>2.	Restaurants & Coffee shops Category</h5>
                        <ul class="sf-list ">
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading Restaurant of the Year</span></li>
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading Restaurant of the Year</span></li>
                        </ul>
                        <h5>3.	Bars and Night Clubs Category</h5>
                        <ul class="sf-list ">
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading Bar and Night Club of the year</span></li>
                        </ul>
                        <h5>4.	Apartment category</h5>
                        <ul class="sf-list ">
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading Apartment of the Year </span></li>
                        </ul>
                        <h5>5.	Conference and event venue Category</h5>
                        <ul class="sf-list ">
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading Conferencing and event venue of the year</span></li>
                        </ul>
                        <h5>6.	Travel Agencies category</h5>
                        <ul class="sf-list ">
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading Travel Agency of the year</span></li>
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading Female owned Travel Agency of the year </span></li>
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading Youth owned Travel Agency of the year </span></li>
                        </ul>
                        <h5>7.	Tour & Drive Guides category </h5>
                        <ul class="sf-list ">
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading female tour guide of the year</span></li>
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading tour guide of the year</span></li>
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading youth tour guide of the year</span></li>
                        </ul>
                        <h5>8.	Community Based Tourism Initiatives Category</h5>
                        <ul class="sf-list ">
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading Community Based Tourism Initiative of the year</span></li>
                        </ul>
                        <h5>9.	Tour operators Category</h5>
                        <ul class="sf-list ">
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading tour operator of the year </span></li>
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading Youth tour operator of the year</span></li>
                            <li><i class="sf-icon-right-chevron"></i><span>- Leading Female Tour Operator of the year</span></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

    </content>
    @include('layouts.footer')
@endsection