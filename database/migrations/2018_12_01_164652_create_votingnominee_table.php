<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotingnomineeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votingnominee', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nominee_id');
            $table->string('nominee_category');
            $table->string('nominee_sms_code');
            $table->string('voterphonenumber');
            $table->string('voternames');
            $table->string('voteremail');
            $table->string('votestatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votingnominee');
    }
}
