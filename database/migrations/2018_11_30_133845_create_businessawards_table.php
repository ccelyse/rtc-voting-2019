<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessawardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businessawards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomineecompanyname');
            $table->string('awardcategory');
            $table->string('people_sub_award');
            $table->string('experience');
            $table->string('sender_name');
            $table->string('sender_email');
            $table->string('sender_phonenumber');
            $table->string('nomineekey');
            $table->string('nomineestatus');
            $table->string('nomineepicture');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('businessawards');
    }
}
