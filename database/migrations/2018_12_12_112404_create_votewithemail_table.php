<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotewithemailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votewithemail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nominee_id');
            $table->string('nominee_category');
            $table->string('voteremail');
            $table->string('votestatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votewithemail');
    }
}
