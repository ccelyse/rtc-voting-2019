<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleawardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peopleawards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nameofnominee');
            $table->string('establishment');
            $table->string('nomineecategory');
            $table->string('people_sub_cate');
            $table->string('experience');
            $table->string('sender_name');
            $table->string('sender_email');
            $table->string('sender_phonenumber');
            $table->string('nomineekey');
            $table->string('nomineestatus');
            $table->string('nomineepicture');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peopleawards');
    }
}
