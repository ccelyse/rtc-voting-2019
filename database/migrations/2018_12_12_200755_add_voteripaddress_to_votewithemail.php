<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVoteripaddressToVotewithemail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('votewithemail', function($table) {
            $table->string('voter_ipaddress');
            $table->string('voter_country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('votewithemail', function($table) {
            $table->string('voter_ipaddress');
            $table->string('voter_country');
        });
    }
}
